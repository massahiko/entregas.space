
<?php 
// Iniciamos nossa sessão que vai indicar o usuário pela session_id
session_start();
include 'complemento/conexao.php';
// Recuperamos os valores passados por parametros
$acao = $_GET['acao']; 
$cod =  $_GET['cod'];
$categoria =  $_GET['categoria'];
$img =  $_GET['img'];
$restaurante = $_GET['restaurante'];
$carrinho = $_GET['carrinho'];
$tamanhopizza = $_GET['tamanhopizza'];

// Verificamos se a acao é igual a incluir
if ($acao == "incluir")
{
   
   // Verificamos se cod do produto é diferente de vazio 
   if ($cod != '')
   {
        
       // Se for diferente de vazio verificamos se é numérico
       //if (is_numeric($cod)) 
       //{    
           // Tratamos a variavel de caracteres indevidos
           $cod = addslashes(htmlentities($cod)); 

           // Verificamos se o produto referente ao $cod já está no carrinho para o session id correnpondente
           $query_rs_carrinho = "SELECT * FROM tbl_carrinho WHERE tbl_carrinho.cod = '".$cod."'  AND tbl_carrinho.sessao = '".session_id()."'";
           $rs_carrinho = mysqli_query($conn, $query_rs_carrinho);
           $row_rs_carrinho = mysqli_fetch_array($rs_carrinho);
           $totalRows_rs_carrinho = mysqli_num_rows($rs_carrinho);
		    
           // Se o total for igual a zero é sinal que o produto ainda não está no carrinho
          // if ($totalRows_rs_carrinho == 0)
          // {
               
               if ($categoria == "P" ){
                   $query_rs_produto = "select * from pizza where id_pizza = '".$cod."'";
                   $rs_produto = mysqli_query($conn, $query_rs_produto);
                   $row_rs_produto = mysqli_fetch_array($rs_produto);
                   $totalRows_rs_produto = mysqli_num_rows($rs_produto);
                   $id = $row_rs_produto['id_pizza'];
                   $nome = $row_rs_produto['sabor'];
                   //$preco = $row_rs_produto['valorgrande'];
                   
                   if(isset($_GET['valorpequena'.$tamanhopizza]))
                   {
                       $tamanho ="PEQUENA";
                       $preco = $row_rs_produto[''];
                   }
                   elseif(isset($_GET['valormedia'.$tamanhopizza])) 
                   {
                        $tamanho ="MEDIA";
                        $preco = $row_rs_produto['valormedia'];
                   } 
                   else
                   {
                        $tamanho ="GRANDE";
                        $preco = $row_rs_produto['valorgrande'];
                   } 
               }elseif($categoria == "E"){
                   $query_rs_produto = "select * from esfiha where id_esfiha = '".$cod."'";
                   $rs_produto = mysqli_query($conn, $query_rs_produto);
                   $row_rs_produto = mysqli_fetch_array($rs_produto);
                   $totalRows_rs_produto = mysqli_num_rows($rs_produto);
                   $id = $row_rs_produto['id_esfiha'];
                   $nome = $row_rs_produto['sabor'];
                   $preco = $row_rs_produto['valor'];
               }elseif($categoria == "B"){
                   $query_rs_produto = "select * from bebida where id_bebida = '".$cod."'";
                   $rs_produto = mysqli_query($conn, $query_rs_produto);
                   $row_rs_produto = mysqli_fetch_array($rs_produto);
                   $totalRows_rs_produto = mysqli_num_rows($rs_produto);
                   $id = $row_rs_produto['id_bebida'];
                   $nome = $row_rs_produto['nome'];
                   $preco = $row_rs_produto['valor'];
               }
               // Aqui pegamos os dados do produto a ser incluido no carrinho
               
               
               
               // Se total for maior que zero esse produto existe e então podemos incluir no carrinho
               if ($totalRows_rs_produto > 0)
               { 
                  
                   // Incluimos o produto selecionado no carrinho de compras
                   $add_sql = "INSERT INTO tbl_carrinho (id, cod, nome, preco, qtd, sessao, img, categoria,tamanho) 
                   VALUES
                   ('','".$id."','".$nome."','".$preco."','1','".session_id()."','".$img."','".$categoria."','".$tamanho."')";
                   $rs_produto_add = mysqli_query($conn, $add_sql) or die(mysqli_error());
             //  }
           // echo $add_sql."<br/>";
            // Verificamos se o produto referente ao $cod já está no carrinho para o session id correnpondente
           $query_rs_carrinho = "SELECT Max(id) as id FROM tbl_carrinho WHERE sessao = '".session_id()."'";
           $rs_carrinho = mysqli_query($conn, $query_rs_carrinho);
           $row_rs_carrinho = mysqli_fetch_array($rs_carrinho);
            
            $SQL2 = "SELECT * FROM borda";
            $res2 = mysqli_query($conn,$SQL2) or 
            				die("Erro na consulta10");
            while ($row2 = mysqli_fetch_assoc($res2)){
            	$id_borda = $row2['id_borda'];
            	
            	if(isset($_GET['borda'.$id_borda]))
                {
                    //echo "checkbox marcado! <br/>";
                    $borda =  $id_borda ;
                }
                else
                {
                    $borda = "";
                  //  echo "checkbox não marcado! <br/>";
                }               
                
                
                if ($borda != '') 
                { 
                   // Se for diferente de vazio verificamos se é numérico
                   if (is_numeric($borda))
                   {    
                       // echo $borda;
                       // Tratamos a variavel de caracteres indevidos
                       $borda = addslashes(htmlentities($borda));
                       
                       // Incluimos o produto selecionado no carrinho de compras
                       $add_sql = "INSERT INTO tbl_carrinho_adicionais (id_carrinho, id_borda, sessao) 
                       VALUES
                       ('".$row_rs_carrinho['id']."', '".$borda."','".session_id()."')";
                       $rs_produto_add = mysqli_query($conn, $add_sql) or die(mysqli_error());
                      // echo $add_sql."<br/>";
                   }
                } 
            }
       }
   }
}

// Verificamos se a acao é igual a excluir
if ($acao == "excluir")
{
   // Verificamos se cod do produto é diferente de vazio
   if ($cod != '')
   {
       // Se for diferente de vazio verificamos se é numérico
       if (is_numeric($cod))
       {    
           // Tratamos a variavel de caracteres indevidos
           $cod = addslashes(htmlentities($cod));
           // Verificamos se o produto referente ao $cod  está no carrinho para o session id correnpondente
           $query_rs_car = "SELECT * FROM tbl_carrinho WHERE id = '".$cod."'  AND sessao = '".session_id()."'";
           $rs_car = mysqli_query($conn, $query_rs_car) or die(mysqli_error());
           $row_rs_carrinho = mysqli_fetch_array($rs_car);
           $totalRows_rs_car = mysqli_num_rows($rs_car);

           // Se encontrarmos o registro, excluimos do carrinho
           if ($totalRows_rs_car > 0)
           {
               $sql_carrinho_excluir = "DELETE FROM tbl_carrinho WHERE id = '".$cod."' AND sessao = '".session_id()."'";    
               $exec_carrinho_excluir = mysqli_query( $conn, $sql_carrinho_excluir) or die(mysqli_error());
           
               $sql_carrinho_adicionais_excluir = "DELETE FROM tbl_carrinho_adicionais WHERE id_carrinho = '".$cod."' AND sessao = '".session_id()."'";    
               $exec_carrinho_adicionais_excluir = mysqli_query( $conn, $sql_carrinho_adicionais_excluir) or die(mysqli_error());
           }
       }
   }
}

// Verificamos se a acao é igual a excluir
if ($acao == "Limpar")
{

           // Verificamos se o produto referente ao $cod  está no carrinho para o session id correnpondente
           $query_rs_car = "SELECT * FROM tbl_carrinho WHERE  sessao = '".session_id()."'";
           $rs_car = mysqli_query($conn, $query_rs_car) or die(mysqli_error());
           $row_rs_carrinho = mysqli_fetch_array($rs_car);
           $totalRows_rs_car = mysqli_num_rows($rs_car);

           // Se encontrarmos o registro, limpamos o carrinho
           if ($totalRows_rs_car > 0)
           {
               $sql_carrinho_excluir = "DELETE FROM tbl_carrinho WHERE sessao = '".session_id()."'";    
               $exec_carrinho_excluir = mysqli_query( $conn,$sql_carrinho_excluir) or die(mysqli_error());
               $sql_carrinho_adicionais_excluir = "DELETE FROM tbl_carrinho_adicionais WHERE sessao = '".session_id()."'";    
               $exec_carrinho_adicionais_excluir = mysqli_query( $conn,$sql_carrinho_adicionais_excluir) or die(mysqli_error());
           }
}

// Verificamos se a ação é de modificar a quantidade do produto
if ($acao == "modifica")
{
   $quant = $_GET['qtd'];
       // Se for diferente de vazio verificamos se é numérico
       if (is_array($quant))
       {    
           // Aqui percorremos o nosso array
           foreach($quant as $cod => $qtd)
           {
               // Verificamos se os valores são do tipo numeric
               if(is_numeric($cod) && is_numeric($qtd))
               {
                   // Fazemos nosso update nas quantidades dos produtos
                   $sql_modifica = "UPDATE tbl_carrinho SET qtd =     '$qtd' WHERE  cod = '$cod' AND sessao = '".session_id()."'";
                   $rs_modifica = mysqli_query($conn, $sql_modifica) or die(mysqli_error());
               }
           }
       }

}
/*
if(isset($_GET['acao']) && isset($_GET['cod']) ) {

         
        sleep(2); //apagar
         
        $html = "<h2>Este é o conteúdo requisitado para demonstrar o método <code>serialize()</code></h2>";
        $html .= "<p>Os dados enviados foram:</p>";
        $html .= "<ul>";
        $html .= "<li>Nome: <strong>$acao</strong></li>";
        $html .= "<li>Cidade: <strong>$cod</strong>";
        $html .= "<li>Cidade: <strong>$restaurante</strong>";
        $html .= "<li>Cidade: <strong>$img</strong>";
        $html .= "<li>Cidade: <strong>$add_sql</strong>";
            $SQL2 = "SELECT * FROM borda";
            $res2 = mysqli_query($conn,$SQL2) or 
            				die("Erro na consulta10");
            while ($row2 = mysqli_fetch_assoc($res2)){
            	$id_borda = $row2['id_borda'];
            	
            	if(isset($_GET['borda'.$id_borda]))
                {
                    //echo "checkbox marcado! <br/>";
                    $html .= "<li>Borda: <strong>$id_borda</strong>";   ;
                }
                else
                {
                  //  echo "checkbox não marcado! <br/>";
                  $html .= "<li>Borda N: <strong>$id_borda</strong>";   ;
                }               
            }
        $html .= "</ul>";
         
        echo $html;
     
    } else {
     
        echo "<script type='text/javascript'>alert('Por favor preencha os campos Nome e Cidade')</script>"; 
         
    }
*/
if( $carrinho == true){
    $url = 'carrinho.php?restaurante='.$restaurante;
    echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';
}else{
    //$url = 'index.php?restaurante='.$restaurante;  
    echo "R$ 25,00";
}
//echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';

?>