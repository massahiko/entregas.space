<?php session_start();?>
<?php
if (isset($_SESSION["id"]) == 0){

 header("location: login.php");

} 
?>
<html>
<head>
    <title>Cadastro de Administrador</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
    <div class="topo">
        <?php
            include('head.php');
        ?>
    </div>
        
    <div class="menutop">
    </div>

    <div class="conteudo">
		<div class="colunamenu">
            <?php
                include('submenu.php');
            ?>
	    </div>
		<div class="center"> 
            <h1>Cadastro de Administrador</h1>
		    <br>
		    <table border="0">
		        <form action="cadAdmin.php" method="post" enctype="multipart/form-data"name="cadAdmin">

			    <tr>
				    <td>Nome:
				    <td><input type="text" name="nome" size="60" style="text-transform:uppercase"/>
			    </tr>
			    <tr>
				    <td>Senha:
				    <td><input type="password" name="senha"  />
			    </tr>
    			<tr>  
    				<td  align="center"><input type="submit" name="salvar" value="Salvar"/>
    				<td> <input type="reset" name="limpar" value="Limpar Dados"/>
    			</tr>
		        </form>
		    </table>
        </div>
	</div>
</body>
</html>