<?php session_start();?>
<?php
if (isset($_SESSION["id"]) == 0){

 header("location: login.php");

} 

?>
<html>
	<head>
		<title>Bordas</title>
	    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="stylesheet" href="css/style.css">

    </head>
	<body>

        <div class="conteudo">
    		<div class="colunamenu">
                <?php
                    include('comp/submenu.php');
                ?>
    		</div>
    		<div class="center"> 
                <h2>Lista de Bordas</h2>
                <br/>
    			<table border="1" cellspacing="0"> 
    			    <form action="formCadBorda.php" method="post" enctype="multipart/form-data">
                    <tr>
    	                <td  align="left" colspan="14"><input type="submit" name="Cadastrar" value="Cadastrar Nova Borda"/>
    
                    </tr>
    				<tr>
    					<th>Cod Borda</th>
                        <th>Recheio</th>
                        <th>Valor </th>
                        <th></th>
                        <th></th>
    				</tr>
    
                    <?php
    
                        include '../complemento/conexao.php';
                        
                    	$SQL = "  SELECT *";
                    	$SQL .= " FROM borda";
                    	$SQL .= " ORDER BY recheio";
                    	$res = mysqli_query($conn,$SQL) or 
                            die("Erro na consulta");
                        $linha = mysqli_num_rows($conn);
                        
                        while ($row = mysqli_fetch_assoc($res)){
                            $id =$row['id_borda'];
                            $recheio = $row['recheio'];
                            $Valor = $row['valor'];
                            
                            echo "<tr>";
                                echo "<td>" . $id . "</td>";
                                echo "<td>" . $recheio . "</td>";
                                echo "<td>" . $Valor . "</td>";

                                echo "<td><a href='formEdtBorda.php?id_borda=$id'><img src=\"img/editar.ico \"></a></td>";
                                echo "<td><a href='delBorda.php?id_borda= $id'><img src=\"img/excluir1.png \"></a></td>";
                            echo "</tr>";
                        }?>						
    
                    </form>
                </table>   
    		</div>
		</div>
	</body>
</html>

