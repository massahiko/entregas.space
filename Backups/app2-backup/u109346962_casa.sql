-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 21-Set-2019 às 13:18
-- Versão do servidor: 10.2.23-MariaDB
-- versão do PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u109346962_casa`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nome` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `senha` varchar(32) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `admin`
--

INSERT INTO `admin` (`id_admin`, `nome`, `senha`) VALUES
(2, 'TESTE', 'c4ca4238a0b923820dcc509a6f75849b'),
(3, 'ALEX', '1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `bebida`
--

CREATE TABLE `bebida` (
  `id_bebida` int(11) NOT NULL,
  `id_rest` int(11) NOT NULL,
  `img` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `nome` varchar(60) CHARACTER SET utf8 NOT NULL,
  `valor` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `bebida`
--

INSERT INTO `bebida` (`id_bebida`, `id_rest`, `img`, `nome`, `valor`) VALUES
(3, 2, '39ce1f2177e4e3d1186da0ad30c64544.png', 'Fanta', '7.00'),
(6, 2, 'a415d988b168a26d368f35b6cb233d60.jpg', 'Pepsi', '3.00'),
(7, 2, 'dfbf653d050215d3a3506bedb69ecb6e.png', 'Fanta', '6.00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `borda`
--

CREATE TABLE `borda` (
  `id_borda` int(11) NOT NULL,
  `recheio` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `borda`
--

INSERT INTO `borda` (`id_borda`, `recheio`, `valor`) VALUES
(1, 'Borda de Catupy', '5.00'),
(4, 'Bacon', '7.00'),
(5, 'Mussarela', '4.00'),
(6, 'maionese', '0.00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `esfiha`
--

CREATE TABLE `esfiha` (
  `id_esfiha` int(11) NOT NULL,
  `id_rest` int(11) NOT NULL,
  `img` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `ingredientes` varchar(120) CHARACTER SET utf8 DEFAULT NULL,
  `sabor` varchar(60) CHARACTER SET utf8 NOT NULL,
  `valor` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `esfiha`
--

INSERT INTO `esfiha` (`id_esfiha`, `id_rest`, `img`, `ingredientes`, `sabor`, `valor`) VALUES
(4, 2, 'c360f81bcbe066b6ee73e52793d85fd3.png', 'Carne', 'Brasil', '6.00'),
(5, 2, ' carne.jpg', 'Esfiha aberta feita com carne bovina, tomate, cebola,limão e tempero árabe.', 'Esfiha de Carne', '6.00'),
(6, 2, '73c8985844ba83badd45ec297af156be.jpg', '200ml', 'Coca', '6.00'),
(7, 2, ' frango.jpg', 'Frango desfiado', 'Frango', '65.00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pizza`
--

CREATE TABLE `pizza` (
  `id_pizza` int(11) NOT NULL,
  `sabor` varchar(60) NOT NULL,
  `ingredientes` varchar(120) DEFAULT NULL,
  `valormeia` decimal(10,2) NOT NULL,
  `valorpequena` decimal(10,2) NOT NULL,
  `valormedia` decimal(10,2) NOT NULL,
  `valorgrande` decimal(10,2) NOT NULL,
  `id_rest` int(11) NOT NULL,
  `img` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pizza`
--

INSERT INTO `pizza` (`id_pizza`, `sabor`, `ingredientes`, `valormeia`, `valorpequena`, `valormedia`, `valorgrande`, `id_rest`, `img`) VALUES
(1, 'Moda da Casa', 'Molho de tomate, mussarela, presunto, frango, calabresa, bacon, milho, pimentão, tomate, cebola, azeitona e orégano.', '10.00', '5.00', '15.00', '20.00', 1, '0a3092ac824024f467616c6ef6a20508.jpg'),
(2, 'teste', 'ters', '2.00', '3.00', '4.00', '5.00', 1, '7ec35efdeb0c91b729c2dac336deb084.jpg'),
(3, 'Xbacon', 'bife, mussarela, bacon', '10.00', '20.00', '40.00', '50.00', 1, '0aa0bcb1eb249562be44f39eb9fc86a1.jpg'),
(7, '#01', ' • Pão de ervas • Carne bovina desfiada • Queijo cheddar • Manjericão • Pimenta • Cebola caramelizada ', '0.00', '0.00', '0.00', '18.00', 2, '4f553a980b2861435022317cce4302e2.jpg'),
(8, '#02 ', '• Pão artesanal • Carne de frango • Vinagrete • Queijo branco ', '0.00', '0.00', '0.00', '16.00', 2, 'ce20447e75ca1e711d18dd940e23a7c5.jpg'),
(9, '#03', '• Pão artesanal • Salsicha de porco com fatias de bacon enrolado • Tomate • Cebola roxa • Alface • Queijo cheddar • Peda', '0.00', '0.00', '0.00', '22.00', 2, '28cb61c0ad552a9ebb57057e1e10b5a2.jpg'),
(10, '#04 ', '• Pão artesanal • Carne bovina recheada com queijo cheddar • Pimentão vermelho e amarelo • Cebola roxa • Queijo cheddar ', '0.00', '0.00', '0.00', '22.00', 2, '240b9ab4ed6e9c4a27401015ea7f9cd1.jpg'),
(11, '#05 ', '• Pão artesanal • Tomate • Alface • Cebola roxa • Peito de peru • Queijo branco', '0.00', '0.00', '0.00', '20.00', 2, 'd0352eb4b96e7a8b584b7c2e002df3cf.jpg'),
(12, '#06 ', '• Pão artesanal • Banana • Chocolate ao leite • Leite condensado • Morango ', '0.00', '0.00', '0.00', '15.00', 2, 'a38e78d2a8e909dcf322daad7fee595f.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `restaurante`
--

CREATE TABLE `restaurante` (
  `id_rest` int(11) NOT NULL,
  `restaurante` varchar(60) NOT NULL,
  `whatsaap` varchar(30) DEFAULT NULL,
  `responsavel` varchar(60) DEFAULT NULL,
  `logo` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `restaurante`
--

INSERT INTO `restaurante` (`id_rest`, `restaurante`, `whatsaap`, `responsavel`, `logo`) VALUES
(1, 'teste', '5511983987395', 'teste', 'ca020078b5a73c57069546b3b2326483.jpg'),
(2, 'A Dogueria', '33988351047', 'Raul', '87ce048248399c9e02184341ed16b9d6.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_carrinho`
--

CREATE TABLE `tbl_carrinho` (
  `id` int(11) NOT NULL,
  `cod` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nome` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `preco` double(10,2) NOT NULL,
  `qtd` int(11) NOT NULL,
  `sessao` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categoria` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tamanho` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `tbl_carrinho`
--

INSERT INTO `tbl_carrinho` (`id`, `cod`, `nome`, `preco`, `qtd`, `sessao`, `img`, `categoria`, `tamanho`) VALUES
(47, '7', '#01', 18.00, 1, 'e52f17ad927dc263aa7af5d6d36c1c38', '4f553a980b2861435022317cce4302e2.jpg', 'P', NULL),
(48, '8', '#02 ', 16.00, 1, 'e52f17ad927dc263aa7af5d6d36c1c38', 'ce20447e75ca1e711d18dd940e23a7c5.jpg', 'P', NULL),
(46, '9', '#03', 22.00, 1, 'e52f17ad927dc263aa7af5d6d36c1c38', '28cb61c0ad552a9ebb57057e1e10b5a2.jpg', 'P', NULL),
(49, '7', '#01', 18.00, 1, 'ee14cc76ec5e65992118a08093a986ef', '4f553a980b2861435022317cce4302e2.jpg', 'P', NULL),
(50, '8', '#02 ', 16.00, 1, 'ee14cc76ec5e65992118a08093a986ef', 'ce20447e75ca1e711d18dd940e23a7c5.jpg', 'P', NULL),
(51, '7', '#01', 18.00, 1, '6a724b22290874eea1dea0af6c2320ac', '4f553a980b2861435022317cce4302e2.jpg', 'P', NULL),
(52, '8', '#02 ', 16.00, 1, '6a724b22290874eea1dea0af6c2320ac', 'ce20447e75ca1e711d18dd940e23a7c5.jpg', 'P', NULL),
(53, '9', '#03', 22.00, 1, '6a724b22290874eea1dea0af6c2320ac', '28cb61c0ad552a9ebb57057e1e10b5a2.jpg', 'P', NULL),
(54, '12', '#06 ', 15.00, 1, 'ee14cc76ec5e65992118a08093a986ef', 'a38e78d2a8e909dcf322daad7fee595f.jpg', 'P', NULL),
(55, '10', '#04 ', 22.00, 1, 'ee14cc76ec5e65992118a08093a986ef', '240b9ab4ed6e9c4a27401015ea7f9cd1.jpg', 'P', NULL),
(56, '9', '#03', 22.00, 1, 'b1a74f71e3e90d14ecd2f088317f637b', '28cb61c0ad552a9ebb57057e1e10b5a2.jpg', 'P', NULL),
(57, '7', '#01', 18.00, 1, 'b1a74f71e3e90d14ecd2f088317f637b', '4f553a980b2861435022317cce4302e2.jpg', 'P', NULL),
(58, '12', '#06 ', 15.00, 1, 'b1a74f71e3e90d14ecd2f088317f637b', 'a38e78d2a8e909dcf322daad7fee595f.jpg', 'P', NULL),
(59, '11', '#05 ', 20.00, 1, 'b1a74f71e3e90d14ecd2f088317f637b', 'd0352eb4b96e7a8b584b7c2e002df3cf.jpg', 'P', NULL),
(60, '11', '#05 ', 20.00, 1, '459147d9264072b3adeb98bd5b149ebc', 'd0352eb4b96e7a8b584b7c2e002df3cf.jpg', 'P', NULL),
(61, '7', '#01', 18.00, 1, 'c918852541843f8490c2974f29919e69', '4f553a980b2861435022317cce4302e2.jpg', 'P', NULL),
(62, '7', '#01', 18.00, 1, '3e990d4dbc0722e782c5a01927c5c6de', '4f553a980b2861435022317cce4302e2.jpg', 'P', NULL),
(63, '8', '#02 ', 16.00, 1, '3e990d4dbc0722e782c5a01927c5c6de', 'ce20447e75ca1e711d18dd940e23a7c5.jpg', 'P', NULL),
(64, '8', '#02 ', 16.00, 1, '2b18d559ef1306349c38e994b7d88c7f', 'ce20447e75ca1e711d18dd940e23a7c5.jpg', 'P', NULL),
(65, '7', '#01', 18.00, 1, '0e8583330e3bb95ce8fc58d05b8fa87e', '4f553a980b2861435022317cce4302e2.jpg', 'P', NULL),
(66, '7', '#01', 18.00, 1, '9b573fd212e61e80ff7016237fb63662', '4f553a980b2861435022317cce4302e2.jpg', 'P', NULL),
(75, '7', '#01', 18.00, 1, 'd08f0c0767d771f8221fdd55529c39e3', '4f553a980b2861435022317cce4302e2.jpg', 'P', NULL),
(91, '4', 'Brasil', 6.00, 1, '8784ad9bc70d7c81aaaba85f941dcfa5', 'c360f81bcbe066b6ee73e52793d85fd3.png', 'E', NULL),
(90, '7', '#01', 18.00, 1, '8784ad9bc70d7c81aaaba85f941dcfa5', '4f553a980b2861435022317cce4302e2.jpg', 'P', NULL),
(86, '7', '#01', 18.00, 1, 'dfdf048891c66e369ddc53c158d682a3', '4f553a980b2861435022317cce4302e2.jpg', 'P', NULL),
(76, '7', '#01', 18.00, 1, '4b94cff7c4599e2a7df33b831edea7d7', '4f553a980b2861435022317cce4302e2.jpg', 'P', NULL),
(77, '4', 'Brasil', 6.00, 1, 'd08f0c0767d771f8221fdd55529c39e3', 'c360f81bcbe066b6ee73e52793d85fd3.png', 'E', NULL),
(78, '6', 'Pepsi', 3.00, 1, 'd08f0c0767d771f8221fdd55529c39e3', 'a415d988b168a26d368f35b6cb233d60.jpg', 'B', NULL),
(79, '8', '#02 ', 16.00, 1, 'd08f0c0767d771f8221fdd55529c39e3', 'ce20447e75ca1e711d18dd940e23a7c5.jpg', 'P', NULL),
(80, '8', '#02 ', 16.00, 1, '252e9a8a8a9ae17bac7bef3b89e1a068', 'ce20447e75ca1e711d18dd940e23a7c5.jpg', 'P', NULL),
(81, '7', '#01', 18.00, 1, '252e9a8a8a9ae17bac7bef3b89e1a068', '4f553a980b2861435022317cce4302e2.jpg', 'P', NULL),
(82, '6', 'Pepsi', 3.00, 1, '252e9a8a8a9ae17bac7bef3b89e1a068', 'a415d988b168a26d368f35b6cb233d60.jpg', 'B', NULL),
(87, '4', 'Brasil', 6.00, 1, '2ee997d00567c8c73082e8b9bc48ec72', 'c360f81bcbe066b6ee73e52793d85fd3.png', 'E', NULL),
(125, '7', '#01', 18.00, 1, '5cd39a830d52410bfb815e0d52d7da18', '4f553a980b2861435022317cce4302e2.jpg', 'P', 'GRANDE'),
(124, '7', '#01', 18.00, 1, '5cd39a830d52410bfb815e0d52d7da18', '4f553a980b2861435022317cce4302e2.jpg', 'P', 'GRANDE'),
(126, '7', '#01', 18.00, 1, 'ea8402f4f0d3c0423597fee9655a06f6', '4f553a980b2861435022317cce4302e2.jpg', 'P', 'GRANDE'),
(127, '4', 'Brasil', 6.00, 1, 'ea8402f4f0d3c0423597fee9655a06f6', 'c360f81bcbe066b6ee73e52793d85fd3.png', 'E', ''),
(128, '3', 'Fanta', 7.00, 1, 'ea8402f4f0d3c0423597fee9655a06f6', '39ce1f2177e4e3d1186da0ad30c64544.png', 'B', ''),
(129, '8', '#02 ', 16.00, 1, 'ea8402f4f0d3c0423597fee9655a06f6', 'ce20447e75ca1e711d18dd940e23a7c5.jpg', 'P', 'GRANDE'),
(130, '7', '#01', 18.00, 1, 'ea8402f4f0d3c0423597fee9655a06f6', '4f553a980b2861435022317cce4302e2.jpg', 'P', 'GRANDE'),
(131, '8', '#02 ', 16.00, 1, 'ea8402f4f0d3c0423597fee9655a06f6', 'ce20447e75ca1e711d18dd940e23a7c5.jpg', 'P', 'GRANDE'),
(134, '7', '#01', 0.00, 1, '0875b402a9bb3af86be189c740393388', '4f553a980b2861435022317cce4302e2.jpg', 'P', 'PEQUENA'),
(135, '4', 'Brasil', 6.00, 1, '0875b402a9bb3af86be189c740393388', 'c360f81bcbe066b6ee73e52793d85fd3.png', 'E', ''),
(136, '3', 'Fanta', 7.00, 1, '0875b402a9bb3af86be189c740393388', '39ce1f2177e4e3d1186da0ad30c64544.png', 'B', ''),
(137, '8', '#02 ', 16.00, 1, '0875b402a9bb3af86be189c740393388', 'ce20447e75ca1e711d18dd940e23a7c5.jpg', 'P', 'GRANDE'),
(138, '7', '#01', 0.00, 1, 'bf1b7de63a3af98e7517af73c94c1fd6', '4f553a980b2861435022317cce4302e2.jpg', 'P', 'MEDIA'),
(139, '7', '#01', 0.00, 1, 'bf1b7de63a3af98e7517af73c94c1fd6', '4f553a980b2861435022317cce4302e2.jpg', 'P', 'MEDIA'),
(141, '4', 'Brasil', 6.00, 1, '5b6e4f226fa0981c9bd3eb51f857a6c2', 'c360f81bcbe066b6ee73e52793d85fd3.png', 'E', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_carrinho_adicionais`
--

CREATE TABLE `tbl_carrinho_adicionais` (
  `id` int(11) NOT NULL,
  `id_borda` int(11) NOT NULL,
  `sessao` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_carrinho` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `tbl_carrinho_adicionais`
--

INSERT INTO `tbl_carrinho_adicionais` (`id`, `id_borda`, `sessao`, `id_carrinho`) VALUES
(14, 5, 'e52f17ad927dc263aa7af5d6d36c1c38', 46),
(15, 1, 'e52f17ad927dc263aa7af5d6d36c1c38', 47),
(16, 4, 'e52f17ad927dc263aa7af5d6d36c1c38', 47),
(17, 5, 'e52f17ad927dc263aa7af5d6d36c1c38', 47),
(18, 1, 'e52f17ad927dc263aa7af5d6d36c1c38', 48),
(19, 4, 'e52f17ad927dc263aa7af5d6d36c1c38', 48),
(20, 5, 'e52f17ad927dc263aa7af5d6d36c1c38', 48),
(21, 1, 'ee14cc76ec5e65992118a08093a986ef', 50),
(22, 4, 'ee14cc76ec5e65992118a08093a986ef', 50),
(23, 5, 'ee14cc76ec5e65992118a08093a986ef', 50),
(24, 1, '6a724b22290874eea1dea0af6c2320ac', 51),
(25, 4, '6a724b22290874eea1dea0af6c2320ac', 51),
(26, 5, '6a724b22290874eea1dea0af6c2320ac', 51),
(27, 1, '6a724b22290874eea1dea0af6c2320ac', 52),
(28, 4, '6a724b22290874eea1dea0af6c2320ac', 52),
(29, 5, '6a724b22290874eea1dea0af6c2320ac', 52),
(30, 1, '6a724b22290874eea1dea0af6c2320ac', 53),
(31, 6, 'ee14cc76ec5e65992118a08093a986ef', 55),
(32, 1, 'b1a74f71e3e90d14ecd2f088317f637b', 56),
(33, 6, 'b1a74f71e3e90d14ecd2f088317f637b', 57),
(34, 5, 'b1a74f71e3e90d14ecd2f088317f637b', 58),
(35, 1, 'b1a74f71e3e90d14ecd2f088317f637b', 59),
(36, 1, '459147d9264072b3adeb98bd5b149ebc', 60),
(37, 1, 'c918852541843f8490c2974f29919e69', 61),
(38, 4, 'c918852541843f8490c2974f29919e69', 61),
(39, 5, 'c918852541843f8490c2974f29919e69', 61),
(40, 6, 'c918852541843f8490c2974f29919e69', 61),
(41, 1, '3e990d4dbc0722e782c5a01927c5c6de', 62),
(42, 4, '3e990d4dbc0722e782c5a01927c5c6de', 62),
(43, 5, '3e990d4dbc0722e782c5a01927c5c6de', 62),
(44, 6, '3e990d4dbc0722e782c5a01927c5c6de', 62),
(45, 4, '3e990d4dbc0722e782c5a01927c5c6de', 63),
(46, 4, '2b18d559ef1306349c38e994b7d88c7f', 64),
(47, 4, '9b573fd212e61e80ff7016237fb63662', 66),
(53, 4, '8784ad9bc70d7c81aaaba85f941dcfa5', 91),
(54, 4, '8784ad9bc70d7c81aaaba85f941dcfa5', 91),
(55, 4, '8784ad9bc70d7c81aaaba85f941dcfa5', 91),
(85, 4, '5cd39a830d52410bfb815e0d52d7da18', 125),
(86, 4, '5cd39a830d52410bfb815e0d52d7da18', 125),
(87, 4, '5cd39a830d52410bfb815e0d52d7da18', 125),
(88, 4, 'ea8402f4f0d3c0423597fee9655a06f6', 126),
(89, 4, 'ea8402f4f0d3c0423597fee9655a06f6', 126),
(90, 4, 'ea8402f4f0d3c0423597fee9655a06f6', 126),
(91, 4, 'ea8402f4f0d3c0423597fee9655a06f6', 127),
(92, 4, 'ea8402f4f0d3c0423597fee9655a06f6', 127),
(93, 4, 'ea8402f4f0d3c0423597fee9655a06f6', 127),
(94, 4, 'ea8402f4f0d3c0423597fee9655a06f6', 130),
(95, 4, 'ea8402f4f0d3c0423597fee9655a06f6', 130),
(96, 4, 'ea8402f4f0d3c0423597fee9655a06f6', 130),
(97, 4, 'ea8402f4f0d3c0423597fee9655a06f6', 131),
(98, 4, 'ea8402f4f0d3c0423597fee9655a06f6', 131),
(99, 4, 'ea8402f4f0d3c0423597fee9655a06f6', 131),
(108, 1, '0875b402a9bb3af86be189c740393388', 134),
(109, 1, '0875b402a9bb3af86be189c740393388', 134),
(110, 1, '0875b402a9bb3af86be189c740393388', 134),
(111, 1, '0875b402a9bb3af86be189c740393388', 134),
(112, 4, '0875b402a9bb3af86be189c740393388', 135),
(113, 4, '0875b402a9bb3af86be189c740393388', 135),
(114, 4, '0875b402a9bb3af86be189c740393388', 135),
(115, 1, '0875b402a9bb3af86be189c740393388', 137),
(116, 1, '0875b402a9bb3af86be189c740393388', 137),
(117, 1, '0875b402a9bb3af86be189c740393388', 137),
(118, 1, '0875b402a9bb3af86be189c740393388', 137),
(123, 5, '5b6e4f226fa0981c9bd3eb51f857a6c2', 141),
(124, 5, '5b6e4f226fa0981c9bd3eb51f857a6c2', 141);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `bebida`
--
ALTER TABLE `bebida`
  ADD PRIMARY KEY (`id_bebida`);

--
-- Indexes for table `borda`
--
ALTER TABLE `borda`
  ADD PRIMARY KEY (`id_borda`);

--
-- Indexes for table `esfiha`
--
ALTER TABLE `esfiha`
  ADD PRIMARY KEY (`id_esfiha`);

--
-- Indexes for table `pizza`
--
ALTER TABLE `pizza`
  ADD PRIMARY KEY (`id_pizza`);

--
-- Indexes for table `restaurante`
--
ALTER TABLE `restaurante`
  ADD PRIMARY KEY (`id_rest`);

--
-- Indexes for table `tbl_carrinho`
--
ALTER TABLE `tbl_carrinho`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_carrinho_adicionais`
--
ALTER TABLE `tbl_carrinho_adicionais`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bebida`
--
ALTER TABLE `bebida`
  MODIFY `id_bebida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `borda`
--
ALTER TABLE `borda`
  MODIFY `id_borda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `esfiha`
--
ALTER TABLE `esfiha`
  MODIFY `id_esfiha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pizza`
--
ALTER TABLE `pizza`
  MODIFY `id_pizza` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `restaurante`
--
ALTER TABLE `restaurante`
  MODIFY `id_rest` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_carrinho`
--
ALTER TABLE `tbl_carrinho`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `tbl_carrinho_adicionais`
--
ALTER TABLE `tbl_carrinho_adicionais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
