<?php
// Iniciamos nossa sessão que vai indicar o usuário pela session_id
session_start();
include 'complemento/conexao.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags-->
    
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
    
        <meta charset="utf-8">
        <meta name="viewport"
            content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui, viewport-fit=cover">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="mobile-web-app-capable" content="yes">
        <meta property="og:site_name" content="Entregas.space">
        <meta property="og:title" content="Pizzaria Casa">
        <meta property="og:description" content="A melhor pizza da cidade!">
        <meta property="og:image" content="http://entregas.space/pizzaria_casa/img/casadapizza.jpg" />
        <meta property="og:image:type" content="image/jpg">
        <meta property="og:type" content="website">
    
    
        <title>entregas.space</title>
        <link rel="manifest" href="/manifest.json">
        <link rel="stylesheet" href="css/framework7.ios.min.css">
        <link rel="stylesheet" href="css/ionicons.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="shortcut icon" href="img/icone.png" />
        <style>
            button {
                background-color: Transparent;
                background-repeat:no-repeat;
                border: none;
                cursor:pointer;
                overflow: hidden;
                outline:none;
            }
        </style>
    </head>

    <body class="color-theme-pink">
        
        <!--<div id="mensagem_erro"></div>-->
        <div id="insere_aqui"></div>
        <div class="container simpleStore">
            <div class="simpleStore_container"></div>
            <div class="simpleStore_cart_container"></div>
        </div>
        <!-- App root element -->
        <div id="app">
            <div class="views tabs">
                <!-- Main view -->
                <div id="view-today" class="view view-main tab tab-active">
                    <div data-name="home" class="page no-navbar">
                        <!-- Scrollable page content -->
                        <div class="page-content  infinite-scroll-content" data-infinite-distance="70">
                            <div class="ptr-preloader">
                            </div>
                            <div class="block">
    
                                <div class="title-container black">
                                    <span class="title-date color-pink">entregas.space/</span>
                                    <div class="title-with-link">
                                        <h1 class=" brand-title"><?php echo $_GET['restaurante'] ?></h1>
                                        <div class="title-with-link">
                                            <div class="modo-app">
                                                <i id="goFS" class="icon ion-ios-phone-portrait"></i>
                                                <h6>Modo App</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
    	                
            					include 'complemento/conexao.php';
            					
            					$restaurante = $_GET['restaurante'];
            					
            					$sql =  "select * from restaurante WHERE restaurante = '$restaurante' ";
                                
            				    $rs = mysqli_query($conn,$sql)or 
            									die("Erro na consulta7");
            				    while ($row = mysqli_fetch_assoc($rs)){
            				        $id_rest = $row['id_rest'];
            				    }
            				    $totalenviarpizza = 0;
            					$comandoSQL = "SELECT * FROM pizza  WHERE id_rest = $id_rest";
            
            					$res = mysqli_query($conn,$comandoSQL) or 
            									die("Erro na consulta8");
            					while ($row = mysqli_fetch_assoc($res)){
            					    $id_pizza = $row['id_pizza'];
            						$sabor = $row['sabor'];
            						$ingredientes = $row['ingredientes'];
            						$valormeia = str_replace(".",",",$row['valormeia']);
            						$valorpequena = str_replace(".",",",$row['valorpequena']);
            						$valormedia = str_replace(".",",",$row['valormedia']);
            						$valorgrande = str_replace(".",",",$row['valorgrande']);
                                    $imagem = $row['img'];
                                    $adc = $row['adc'];
                                    $totalenviarpizza += 1;
            					?>      
    					        <form method="GET" action="acao.php"  id="ajax_form<?php echo $totalenviarpizza ?>" name="ajax_form<?php echo $totalenviarpizza ?>"><!--<form  method="GET">-->
                                    <!-- Item Inicio -->
                                    <div class="card elevation-18 ">
                                        <img class="card-image item_thumb" src=<?php echo "produtos/pizzas/" . $imagem ?>
                                            alt=""> <!-- imagem do item -->
        
                                        <div class="card-infos ">
        
                                            <!-- Adicional -->
                                            <div class="card-monte">
        
                                            </div>
                                            <?php
                                            if ($adc == 1){
                								$SQL1 = "SELECT * FROM borda";
                
                								$res1 = mysqli_query($conn,$SQL1) or 
                												die("Erro na consulta9");
                								while ($row1 = mysqli_fetch_assoc($res1)){
                									$recheio = $row1['recheio'];
                									$valorborda = $row1['valor'];							
                								?>
                                                <div class="simpleCart_shelfItem">
                                                    <div class="chip-adicional "><i class="icon ion-ios-add-circle-outline"></i>
                                                        <!-- adicionar itens adicionais! -->
            
                                                        <div class="item_price price"> <?php echo "R$" . $valorborda ?> </div>
                                                        <a href="javascript:;" class=" single open-full">
                                                            <div class=" item_add">
                                                                <div class="item_name "><?php echo $recheio ?> </div>
                                                            </div>
                                                        </a>
            
                                                    </div>
                                                </div>
            
                                            <?php 
                                                }
                                            }
                                            ?>
                                            <!-- Fim Adicional -->
                                            <div class="simpleCart_shelfItem">
                                            
        
                                                <h2 class="card-title item_name"><?php echo $sabor ?> </h2>
                                                <!-- Nome do item -->
                                                <div class="card-bottom">
                                                    <div class="card-author">
                                                        <div class=""><?php echo $ingredientes ?> </div>
                                                        <!-- Descrição do item -->
                                                    </div>
                                                </div>
                                                <div class="list list-adicionais">
                                                    <!-- Itens Adicionais -->
                                                    <ul>
                                                        <li class="accordion-item"><a href="#" class="item-content item-link">
                                                                <div class="item-inner">
                                                                    <div class="item-title">Monte seu lanche</div>
                                                                </div>
                                                            </a>
                                                            <div class="accordion-item-content">
                                                                <div class="item-title subtitulo">Adicionais</div>
                                                                <div class="block row padding-bottom">
                                                                    <?php
                                                                    if ($adc == 1){
                                        								$SQL2 = "SELECT * FROM borda";
                                        
                                        								$res2 = mysqli_query($conn,$SQL2) or 
                                        												die("Erro na consulta10");
                                        								while ($row2 = mysqli_fetch_assoc($res2)){
                                        									$recheio = $row2['recheio'];
                                        									$valorborda = str_replace(".",",",$row2['valor']);	
                                        									$id_borda = $row2['id_borda'];
                                        									 
                                        								?>
                    								            
                                                                        <label class="item-checkbox item-content">
                                                                            <input type="checkbox" name="<?php echo "borda".$id_borda?>" />
                                                                            <i
                                                                                class="icon icon-checkbox"><?php 
                                                                                                            echo "R$ " . $valorborda . " - " .  $recheio;
                                                                                                            //echo "<a acao.php?='$id_borda'&acao=atualizar></a>";
                                                                                                            ?>
                                                                            </i>  
                                                                        </label>
            
                                                                    <?php 
                                                                        } 
                                                                    }
                                                                    ?>
                                                                </div>
        
                                                                <div class="item-title subtitulo">Selecione o tamanho</div>
                                                                <div class="block row padding-bottom">
                                                                    <label class="item-checkbox item-content">
                                                                        <input id="valorpequena<?php echo $totalenviarpizza ?>" name="valorpequena<?php echo $totalenviarpizza ?>" type="checkbox" onclick="marcaDesmarca(this.id,<?php echo $totalenviarpizza ?>)"/>
                                                                        <i
                                                                            class="icon icon-checkbox"><?php echo "Pequena R$" . $valorpequena ?></i>
                                                                    </label>
                                                                    <label class="item-checkbox item-content">
                                                                        <input id="valormedia<?php echo $totalenviarpizza ?>" name="valormedia<?php echo $totalenviarpizza ?>" type="checkbox" onclick="marcaDesmarca(this.id,<?php echo $totalenviarpizza ?>)"/>
                                                                        <i class="icon icon-checkbox"><?php echo "Média R$" . $valormedia ?>
                                                                        </i>
                                                                    </label>
                                                                    <label class="item-checkbox item-content">
                                                                        <input id="valorgrande<?php echo $totalenviarpizza ?>" name="valorgrande<?php echo $totalenviarpizza ?>" type="checkbox" onclick="marcaDesmarca(this.id,<?php echo $totalenviarpizza ?>)"/>
                                                                        <i class="icon icon-checkbox"><?php echo "Grande R$" . $valorgrande ?>
                                                                        </i>
                                                                    </label>
                                                                    <div class="padding-bottom-2 item-inner" ></div>
                                                                    <input type="Hidden" name="acao" value="incluir" >
                                                                    <input type="Hidden" name="categoria" value="P" >
                                                                    <input type="Hidden" name="img" value="<?php echo $imagem?>" >
                                                                    <input type="Hidden" name="cod" value="<?php echo $id_pizza?>" >
                                                                    <input type="Hidden" name="restaurante" value="<?php echo $restaurante?>" >
                                                                    <button id="enviar<?php echo $totalenviarpizza ?>" name="enviar<?php echo $totalenviarpizza ?>" type="submit">
                                                                        <div class="post-category open-full item_add row">
                                                                             Adicionar<div id="insere_aqui"></div>
                                                                        </div>
                                                                        <div id="insere_aqui"></div>
                                                                    </button>
                                                                </div>
                                                                    <!--<!<a href="javascript:;" class=" single open-full">
                                                                        <?php// echo "<a href='acao.php?restaurante=$restaurante&borda=&cod=$id_pizza&acao=incluir'>" ?>
                                                                    
                                                                    </a>
                                                                </a> -->
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div><!-- Itens Adicionais -->
        
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <?php } ?>
                                <!-- Item Final -->
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                //	include('especial.php');
                ?>
                <!-- Tela2 View -->
                <div id="view-discover" class="view tab">
                    <div data-name="discover" class="page no-navbar">
                        <!-- Scrollable page content -->
                        <div class="page-content">
    
                            <div class="block">
                                <div class="title-container black">
                                    <span class="title-date color-pink">entregas.space/</span>
                                    <div class="title-with-link">
                                        <h1 class="brand brand-title"></h1>
                                    </div>
                                </div>
                            </div>
    
                            <div id="discover-swiper" class="swiper-container">
                                <div class="swiper-wrapper">
    
                                    <div class="swiper-slide  ">
                                        <?php
                    				    $totalenviaresfiha = 0;
            							$comandoSQL3 = "SELECT * FROM esfiha  WHERE id_rest = $id_rest";
            
            							$res3 = mysqli_query($conn,$comandoSQL3) or 
            											die("Erro na consulta1");
            							while ($row3 = mysqli_fetch_assoc($res3)){
            							    $id_esfiha = $row3['id_esfiha']; 
            								$sabor3 = $row3['sabor'];
            								$ingredientes3 = $row3['ingredientes'];
            								$valor3 = str_replace(".",",",$row3['valor']);
            								$imagem3 = $row3['img'];
                                            $totalenviaresfiha += 1;
            							
            							?>
    							         <form method="GET"  id="ajax_form_esfiha<?php echo $totalenviaresfiha ?>"><!--<form action="acao.php" method="GET">-->
                                        <!-- Item Inicio -->
                                        <div class="card elevation-18 ">
    
                                            <img class="card-image item_thumb"
                                                src="<?php echo "produtos/esfihas/$imagem3" ?>" alt="">
                                            <!-- imagem do item -->
    
                                            <div class="card-infos ">
    
                                                <div class="card-monte">
                                                </div>
                                                <?php
                                                if ($adc == 1){
                        							$SQL1 = "SELECT * FROM borda";
                        
                        							$res1 = mysqli_query($conn,$SQL1) or 
                        											die("Erro na consulta2");
                        							while ($row1 = mysqli_fetch_assoc($res1)){
                        								$recheio = $row1['recheio'];
                        								$valorborda = $row1['valor'];							
                        							?>
                                                        <div class="simpleCart_shelfItem">
                                                            <div class="chip-adicional "><i
                                                                    class="icon ion-ios-add-circle-outline"></i>
                                                                <!-- adicionar itens adicionais! -->
            
                                                                <div class="item_price price"> <?php echo "R$" . $valorborda ?>
                                                                </div>
                                                                <a href="javascript:;" class=" single open-full">
                                                                    <div class=" item_add">
                                                                        <div class="item_name "><?php echo $recheio ?> </div>
                                                                    </div>
                                                                </a>
            
                                                            </div>
                                                        </div>
    
                                                <?php 
                                                    }
                                                }
                                                ?>
                                                <!-- Fim Adicional -->
                                                <div class="simpleCart_shelfItem">
    
                                                    <h2 class="card-title item_name"><?php echo $sabor3 ?></h2>
                                                    <!-- Nome do item -->
                                                    <div class="card-bottom">
                                                        <div class="card-author">
                                                            <div class=""><?php $ingredientes3 ?></div>
                                                            <!-- Descrição do item -->
                                                        </div>
                                                    </div>
                                                    <div class="list list-adicionais">
                                                        <!-- Itens Adicionais -->
                                                        <ul>
                                                            <li class="accordion-item"><a href="#"
                                                                    class="item-content item-link">
                                                                    <div class="item-inner">
                                                                        <div class="item-title">Especiais</div>
                                                                    </div>
                                                                </a>
                                                                <div class="accordion-item-content">
                                                                    <div class="item-title subtitulo">Adicionais</div>
                                                                    <div class="block row padding-bottom">
                                                                        <?php
                                                                        if ($adc == 1){
            																$SQL4 = "SELECT * FROM borda";
            								
            																$res4 = mysqli_query($conn,$SQL4) or 
            																				die("Erro na consulta4");
            																while ($row4 = mysqli_fetch_assoc($res4)){
            																	$recheio4 = $row4['recheio'];
            																	$valorborda4 = str_replace(".",",",$row4['valor']);		
                        									                    $id_borda4 = $row4['id_borda'];
            																	 
            																?>
                                                                                <label class="item-checkbox item-content">
                                                                                    <input type="checkbox" name="<?php echo "borda".$id_borda4?>"/>
                                                                                    <i
                                                                                        class="icon icon-checkbox"><?php echo "R$ " . $valorborda4 . " - " .  $recheio4 ?></i>
                                                                                </label>
            
                                                                        <?php 
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </div>
    
                                                                    <div class="item-title subtitulo">Selecione o tamanho
                                                                    </div>
                                                                    <div class="block row padding-bottom">
    
                                                                        <label class="item-checkbox item-content">
                                                                            <input type="checkbox" />
                                                                            <i
                                                                                class="icon icon-checkbox"><?php echo "Valor R$" . $valor3 ?></i>
                                                                        </label>
    
                                                                    </div>
                                                                    <input type="Hidden" name="acao" value="incluir" >
                                                                    <input type="Hidden" name="categoria" value="E" >
                                                                    <input type="Hidden" name="img" value="<?php echo $imagem3?>" >
                                                                    <input type="Hidden" name="cod" value="<?php echo $id_esfiha?>" >
                                                                    <input type="Hidden" name="restaurante" value="<?php echo $restaurante?>" >
                                                                    <button id="enviar_esfiha<?php echo $totalenviaresfiha ?>()" onclick="enviar_esfiha<?php echo $totalenviaresfiha ?>()">
                                                                        <div class="post-category item_add row">
                                                                             Adicionar
                                                                        </div>
                                                                    </button>
                                                                    <!--<button type="submit">
                                                                        <div class="post-category item_add row">
                                                                             Adicionar
                                                                        </div>
                                                                    </button>
                                                                    <!--    
                                                                    <a href="javascript:;" class=" single open-full">
                                                                        <div class="post-category item_add row">R$23
                                                                            Adicionar</div>
                                                                    </a>
                                                                    -->
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div><!-- Itens Adicionais -->
    
                                                </div>
                                            </div>
    
                                            <!--		</div>
                        <!-- Item Final -->
                                        </div>
                                        </form>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <!-- Categories View -->
                <div id="view-categories" class="view tab">
                    <div data-name="categories" class="page no-navbar">
                        <!-- Scrollable page content -->
                        <div class="page-content">
                            <div class="block">
                                <div class="title-container black">
    
                                    <span class="title-date color-pink">entregas.space/</span>
                                    <div class="title-with-link">
                                        <h1 class="brand brand-title"></h1>
    
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="two-columns-cards">
                                    
                                    <!-- Inicio View -->
                                    <?php
            				    $totalenviarbebida = 0;
                                $comandoSQL5 = "SELECT * FROM bebida  WHERE id_rest = $id_rest";
    
            					$res5 = mysqli_query($conn,$comandoSQL5) or 
            									die("Erro na consulta6");
            					while ($row5 = mysqli_fetch_assoc($res5)){
            					    $id_bebida = $row5['id_bebida'];
            						$nomebebida = $row5['nome'];
            						$valor5 = str_replace(".",",",$row5['valor']);
                                    $imagem5 = $row5['img'];
                                    $totalenviarbebida += 1;
            					?>
            					    <form method="GET"  id="ajax_form_bebida<?php echo $totalenviarbebida ?>" name="ajax_form_bebida<?php echo $totalenviarbebida ?>" ><!--<form action="acao.php" method="GET">-->
                                    <!--<a href="#">-->
                                        <div class="card simpleCart_shelfItem">
                                            <img class="card-image item_thumb"
                                                src="<?php echo "produtos/bebidas/$imagem5" ?>" alt="">
                                            <div class="card-infos">
                                                <h2 class="card-title-drink item_name"><?php echo $nomebebida ?></h2>
                                                <div class="row">
                                                    <div class="add-price item_price"><?php echo "R$ " . $valor5  ?></div>
                                                    <input type="Hidden" name="acao" value="incluir" >
                                                    <input type="Hidden" name="categoria" value="B" >
                                                    <input type="Hidden" name="img" value="<?php echo $imagem5?>" >
                                                    <input type="Hidden" name="cod" value="<?php echo $id_bebida?>" >
                                                    <input type="Hidden" name="restaurante" value="<?php echo $restaurante?>" >
                                                    <button id="enviar_esfiha<?php echo $totalenviarbebida ?>()" onclick="enviar_esfiha<?php echo $totalenviarbebida ?>()">
                                                        <div class="post-category item_add row">
                                                             Adicionar
                                                        </div>
                                                    </button>
                                                    <!--<button type="submit">
                                                        <div class="post-category item_add row">
                                                             Adicionar
                                                        </div>
                                                    </button>
                                                    <!--<div href="javascript:;" class="add-slide2 item_add open-full">adicionar
                                                    </div>-->                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                    <!--</a>-->
                                    </form>
                                    <?php } ?>
                                    <!-- Fim View -->
    
                                </div>
                            </div>
    
                        </div>
                    </div>
                </div>
    <?php /*
                <div class="login-screen">
                    <div class="view">
                        <div class="page">
                            <div class="page-content login-screen-content">
                                <a href="#" class="link login-screen-close close-button">
                                    <img src="img/close.svg" alt="Close">
                                </a>
                                <div class="login-screen-title">Seu Pedido</div>
                                <div class="block search-container simpleStore_cart">
                                    <!-- Cart View -->
    
                                    <div id="cart">
                                        <div id='container'>
                                            <ul class="list media-list post-list block">
                                                <li>
                                                    
                                                    <div class="item-content">
                                                        
                                                        <div class="item-inner">
                                                            <?php
                                                           $sql_meu_carrinho = "SELECT * FROM tbl_carrinho WHERE  sessao = '".session_id()."' ORDER BY nome ASC";
                                                           $exec_meu_carrinho =  mysqli_query($conn,$sql_meu_carrinho) or die(mysql_error());
                                                           $qtd_meu_carrinho = mysqli_num_rows($exec_meu_carrinho);
                                                            
                                                           if ($qtd_meu_carrinho > 0)
                                                           {
                                                                $soma_carrinho = 0;
                                                                while ($row_rs_produto_carrinho = mysqli_fetch_assoc($exec_meu_carrinho))
                                                                {
                                                                    $soma_carrinho += ($row_rs_produto_carrinho['preco']*$row_rs_produto_carrinho['qtd']);
                                                                    $id = $row_rs_produto_carrinho['id'];
                                                                    if($row_rs_produto_carrinho['categoria'] == "P"){
                                                                        $imagem = "produtos/pizzas/".$row_rs_produto_carrinho['img'];     
                                                                    }elseif($row_rs_produto_carrinho['categoria'] == "E"){
                                                                        $imagem = "produtos/esfihas/".$row_rs_produto_carrinho['img'];  
                                                                    }elseif($row_rs_produto_carrinho['categoria'] == "B"){
                                                                        $imagem = "produtos/bebidas/".$row_rs_produto_carrinho['img'];  
                                                                    };
                                                                    
                                                            ?> 
                                                                    <div class="item-media"><img src="<?php echo $imagem ?>" alt=""></div>
                                                                    <div class="row col">
                                                                        <div class="item-subtitle col-45"><?php echo $row_rs_produto_carrinho['nome']?></div>
                                                                        <div class="item-subtitle  col-45">(Grande)</div>
                                                                        <div class="col-20 item-delete"><?php echo "<a href='acao.php?restaurante=$restaurante&cod=$id&acao=excluir'><i
                                                                                class=\"icon ion-md-trash\">excluir</i></a> "?></div>
                                                                    </div>
                                                                    <div class=" bottom-subtitle ">Meia Calabresa</div>
                                                                    <div class="row col">
                                                                        <div class="item-subtitle bottom-subtitle ">1x<div
                                                                                class="padding-10"><?php echo number_format($row_rs_produto_carrinho['preco'],2,",","."); ?></div>
                                                                        </div>
             
                                                                    </div>
                                                                    <?php
                                                                       $sql_meu_carrinho_adicional = "SELECT bor.*, adc.* FROM tbl_carrinho_adicionais as adc,borda as bor WHERE bor.id_borda = adc.id_borda and id_carrinho = $id and  sessao = '".session_id()."'";
                                                                       $exec_meu_carrinho_adicional =  mysqli_query($conn,$sql_meu_carrinho_adicional) or die(mysqli_error());
                                                                        while ($row_rs_produto_carrinho_adicional = mysqli_fetch_assoc($exec_meu_carrinho_adicional))
                                                                        {
                                                                       ?> 
                                                                            <div class="item-subtitle bottom-subtitle"><i
                                                                                    class="icon ion-md-add"></i>
                                                                                <div class="padding-right-10">R$<?php echo number_format($row_rs_produto_carrinho_adicional['valor'],2,",",".")." </div>". $row_rs_produto_carrinho_adicional['recheio']?>
                                                                            </div>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    <!--<div class="item-subtitle bottom-subtitle"><i
                                                                            class="icon ion-md-add"></i>
                                                                        <div class="padding-right-10">R$5</div> Borda de Catupiry
                                                                    </div>-->
                                                                    <div class="item-subtitle bottom-subtitle">Sub-Total: <div
                                                                        class="padding-10"><?php echo number_format($row_rs_produto_carrinho['preco'],2,",",".");?></div>
                                                                    </div>
                                                            <?php
                                                                }
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
    
                                                </li>
    
                                            </ul>
                                            <div class="data-table row">
                                                <?php echo "<a href='acao.php?restaurante=$restaurante&cod=$id_pizza&acao=Limpar' class=\"simpleCart_empty button color-gray u-pull-left\"> "?>
                                                    <!--<a href="javascript:;"
                                                        class="simpleCart_empty button color-gray u-pull-left">-->Limpar Carrinho
                                                        <i class="fa fa-trash-o"></i><!--</a>-->
                                                </a>
                                                <a href="javascript:;" class="button button-primary  u-pull-right">Total:<?php echo number_format($soma_carrinho ,2,",",".");?><!-- <i
                                                        class="fa fa-arrow-right simpleCart_grandTotal"></i>--></a>
                                            </div>
                                            <div class="list no-hairlines custom-form contact-form">
                                                <form class="list ">
                                                    <ul>
                                                        <div class="row pay-top-2">
                                                            <div class="col-60">
                                                                <h5>Formas de Pagamentos</h5>
                                                            </div>
                                                            <div class="col-40">
                                                                <h5>Total:<?php echo number_format($soma_carrinho ,2,",",".");?><!-- <i
                                                                        class="fa fa-arrow-right simpleCart_grandTotal"></i>-->
                                                                </h5>
                                                            </div>
                                                            <div class="col-65  pay-top">
                                                                <li class="item-content item-input">
                                                                    <div class="item-inner">
                                                                        <div class="item-input-wrap">
                                                                            <input type="number"
                                                                                placeholder="Dinheiro, troco para?">
                                                                            <span class="input-clear-button"></span>
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                            <div class="col-35 row  pay-top">
                                                                <h5>Cartão</h5>
                                                                <label class="toggle toggle-init color-pink">
                                                                    <input type="checkbox">
                                                                    <span class="toggle-icon"></span>
                                                                </label>
                                                            </div>
                                                        </div>
    
                                                        <li class="item-content item-input">
                                                            <div class="item-inner">
                                                                <div class="item-input-wrap">
                                                                    <textarea class="resizable"
                                                                        placeholder="Adicione uma observação para seu pedido!"></textarea>
                                                                    <span class="input-clear-button"></span>
                                                                </div>
                                                            </div>
                                                        </li>
    
                                                        <h5>Endereço de Entrega</h5>
                                                        <form class="list form-store-data store-data" id="my-form">
                                                            <li class="item-content item-input">
                                                                <div class="item-inner">
                                                                    <div class="item-input-wrap">
                                                                        <input type="text" name="rua" placeholder="Rua">
                                                                        <span class="input-clear-button"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="item-inner">
                                                                    <div class="item-input-wrap">
                                                                        <input type="number" name="numero"
                                                                            placeholder="Número">
                                                                        <span class="input-clear-button"></span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="item-content item-input">
                                                                <div class="item-inner">
                                                                    <div class="item-input-wrap input-dropdown-wrap ">
                                                                        <select name="cidade">
                                                                            <option value="01">Dom Cavati-MG</option>
    
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="item-inner">
                                                                    <div class="item-input-wrap">
                                                                        <input type="text" name="bairro"
                                                                            placeholder="Bairro">
                                                                        <span class="input-clear-button"></span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </form>
                                                    </ul>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <a onclick='screenshot();' id="button1"
                                        class="big-button promo-banner-2 button button-fill link open-preloader-indicator"
                                        href="#" data-popup=".popup-about">
                                        <i class="icon ion-ios-send "></i>Confirmar Pedido
                                    </a>
                                    <div id="div_links" class="w-button"></div>
    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>*/?>
   
                <!-- Toolbar -->
                
                <div class="toolbar tabbar tabbar-labels">
                    <div class="toolbar-inner">
                        <a href="#view-today" class="tab-link tab-link-active tab-icon">
                            <i class="icon ion-ios-today"></i>
                            <span class="tabbar-label">Feed</span>
                        </a>
                        <a href="#view-discover" class="tab-link">
                            <i class="icon ion-ios-restaurant"></i>
                            <span class="tabbar-label">Especiais</span>
                        </a>
                        <a href="#view-categories" class="tab-link ">
                            <i class="icon ion-ios-cafe"></i>
                            <span class="tabbar-label">Bebidas</span>
                        </a>
                        <a href="#" class="tab-link">
                        </a>
                        <a href="carrinho.php?restaurante=<?php echo $restaurante ?>" class="tab-link login-screen-open" ><!--data-login-screen=".login-screen"-->
                            <i class="icon ion-md-basket"><span
                                    class="badge badge-position color-pink simpleCart_quantity"></span></i>
                            <span class="tabbar-label">Pedido</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    
    
    
    
    
        <!-- Scripts
    
         
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    
    
    
        <script src="js/framework7.min.js"></script>
        <script src="js/app.js"></script>
        <!--<script src="js/all.js"></script>-->
    
        <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="js/simpleCart.min.js"></script>
        <script src="js/simpleStore.js"></script>
        <script src="js/whatsapp.js"></script>
        <script type="text/javascript"
            src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.1/dist/html2canvas.min.js"></script>
    
        <script src="js/config.js"></script>
        <script>
            if ('serviceWorker' in navigator) {
                console.log("Will the service worker register?");
                navigator.serviceWorker.register('service-worker.js')
                    .then(function (reg) {
                        console.log("Yes, it did.");
                    }).catch(function (err) {
                        console.log("No it didn't. This happened:", err)
                    });
            }
        </script>
        <script>
            var goFS = document.getElementById("goFS");
            goFS.addEventListener("click", function () {
                document.body.requestFullscreen();
            }, false);
        </script>
        <script  type="text/javascript">
 
            function marcaDesmarca(id,tamanho) {
            //alert(id+"-"+tamanho);
              if (id == "valorpequena"+tamanho) {
                document.getElementById('valormedia'+tamanho).checked = false;
                document.getElementById('valorgrande'+tamanho).checked = false;
              } else if (id == "valormedia"+tamanho) {
                document.getElementById('valorpequena'+tamanho).checked = false;
                document.getElementById('valorgrande'+tamanho).checked = false;
              } else if (id == "valorgrande"+tamanho) {
                document.getElementById('valorpequena'+tamanho).checked = false;
                document.getElementById('valormedia'+tamanho).checked = false;
              }
            };
            

        </script>
        <script type="text/javascript">
        var iconCarregando = $('<span class="destaque">Carregando. Por favor aguarde...</span>');
        //var enviar=1
        //alert(enviar);
        //while (enivar < 6){
        //    enviar++;
        for (enviar=1; enviar<=999; enviar++){
            //alert(enviar);
            jQuery('#ajax_form'+enviar).submit(function(e) {
                e.preventDefault();
                var serializeDados = jQuery( this ).serialize();
                 
                $.ajax({
                    url: 'acao.php',
                    dataType: 'html',
                    type: 'GET',
                    data: serializeDados,
                    beforeSend: function() {
                        $('#insere_aqui').html(iconCarregando); 
                    },
                    complete: function() {
                        $(iconCarregando).remove(); 
                    },
                    success: function(data, textStatus) {
                        $('#insere_aqui').html('<p>' + data + '</p>');  
                    },
                    error: function(xhr,er) {
                        $('#mensagem_erro').html('<p class="destaque">Error ' + xhr.status + ' - ' + xhr.statusText + '<br />Tipo de erro: ' + er + '</p>')
                    } 
                });     
                 
            });
        };
    </script>


    <script type="text/javascript">
        var iconCarregando1 = $('<span class="destaque">Carregando. Por favor aguarde...</span>');
       // var enviar_esfiha;
        //alert(enviar_esfiha);
        //while (enivar_esfiha< 10){
         //   enviar_esfiha++;
         
        for (enviar_esfiha=1; enviar_esfiha<=999; enviar_esfiha++){
            //alert(enviar_esfiha);
            jQuery('#ajax_form_esfiha'+enviar_esfiha).submit(function(e) {
                e.preventDefault();
                var serializeDados = jQuery( this ).serialize();
                 
                $.ajax({
                    url: 'acao.php',
                    dataType: 'html',
                    type: 'GET',
                    data: serializeDados,
                    beforeSend: function() {
                        $('#insere_aqui').html(iconCarregando1); 
                    },
                    complete: function() {
                        $(iconCarregando1).remove(); 
                    },
                    success: function(data, textStatus) {
                        $('#insere_aqui').html('<p>' + data + '</p>');  
                    },
                    error: function(xhr,er) {
                        $('#mensagem_erro').html('<p class="destaque">Error ' + xhr.status + ' - ' + xhr.statusText + '<br />Tipo de erro: ' + er + '</p>')
                    } 
                });     
                 
            });
        };
    </script>
    <script type="text/javascript">
        var iconCarregando2 = $('<span class="destaque">Carregando. Por favor aguarde...</span>');
        var enviar_bebida = 0;
        //alert(enviar_bebida);
       // while (enviar_bebida< 10){
       //     enviar_bebida++;
        for (enviar_bebida=1; enviar_bebida<=999; enviar_bebida++){
            //alert(enviar_bebida);
            jQuery('#ajax_form_bebida'+enviar_bebida).submit(function(e) {
                e.preventDefault();
                var serializeDados = jQuery( this ).serialize();
                 
                $.ajax({
                    url: 'acao.php',
                    dataType: 'html',
                    type: 'GET',
                    data: serializeDados,
                    beforeSend: function() {
                        $('#insere_aqui').html(iconCarregando2); 
                    },
                    complete: function() {
                        $(iconCarregando2).remove(); 
                    },
                    success: function(data, textStatus) {
                        $('#insere_aqui').html('<p>' + data + '</p>');  
                        $('#valorsubtotal').html('<p>' + data + '</p>');
                    },
                    error: function(xhr,er) {
                        $('#mensagem_erro').html('<p class="destaque">Error ' + xhr.status + ' - ' + xhr.statusText + '<br />Tipo de erro: ' + er + '</p>')
                    } 
                });     
                 
            });
        };
    </script>
    </body>

</html>
