<?php session_start();?>
<?php
if (isset($_SESSION["id"]) == 0){

 header("location: login.php");

} 
?>
<html>
<head>
    <title>Cadastro de Restaurante</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>


    <div class="conteudo">
		<div class="colunamenu">
            <?php
                include('submenu.php');
            ?>
	    </div>
		<div class="center"> 
            <h1>Cadastro de Restaurante</h1>
		    <br>
		    <table border="0">
		        <form action="cadRest.php" method="post" enctype="multipart/form-data"name="cadRest">

			    <tr>
				    <td>Restaurante:
				    <td><input type="text" name="restaurante" size="60" />
			    </tr>
			    <tr>
				    <td>WhatsAap:
				    <td><input type="text" name="whatsaap" size="120" />
			    </tr>
			    <tr>
				    <td>Responsavel:
				    <td><input type="text" name="responsavel" size="14" />
			    </tr>
			    <tr>
			        
    			    <td>Cidade:</td> 
			        <td>
    			        <select name="cidade">
                            <option value="01">Dom Cavati-MG</option>
                            <?php
    				            include '../complemento/conexao.php';
    				            $sql =  "select * from cidade group by descricao";
    				            $rs = mysqli_query($conn,$sql);
    				            while($reg = mysqli_fetch_object($rs)):
    				        ?>
    				            <option value="<?php echo $reg->descricao."-".$reg->uf ?>"><?php echo $reg->descricao."-".$reg->uf ?></option>
    				        <?php
    				            endwhile;
    				        ?>            
                        </select>
                    </td>
                </tr>
			    <tr>
				    <td>logo:
                    <td><input type="file" name="logo" value="" multiple/>
			    </tr>			    
    			<tr>  
    				<td  align="center"><input type="submit" name="salvar" value="Salvar"/>
    				<td> <input type="reset" name="limpar" value="Limpar Dados"/>
    			</tr>
		        </form>
		    </table>
        </div>
	</div>
</body>
</html>