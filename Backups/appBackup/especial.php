  <div id="view-discover" class="view tab">
        <div data-name="discover" class="page no-navbar">
            <!-- Scrollable page content -->
            <div class="page-content">

                <div class="block">
                    <div class="title-container black">
                        <span class="title-date color-pink">entregas.space/</span>
                        <div class="title-with-link">
                            <h1 class="brand brand-title"></h1>
                        </div>
                    </div>
                </div>

                <div id="discover-swiper" class="swiper-container">
                    <div class="swiper-wrapper">

                        <div class="swiper-slide  ">
                            <?php
					$comandoSQL3 = "SELECT * FROM esfiha  WHERE id_rest = $id_rest";

					$res3 = mysqli_query($conn,$comandoSQL3) or 
									die("Erro na consulta1");
					while ($row3 = mysqli_fetch_assoc($res3)){
					    $id_esfiha = $row3['id_esfiha']; 
						$sabor3 = $row3['sabor'];
						$ingredientes3 = $row3['ingredientes'];
						$valor3 = str_replace(".",",",$row3['valor']);
						$imagem3 = $row3['img'];
					
					?>
					        <form action="acao.php" method="GET">
                            <!-- Item Inicio -->
                            <div class="card elevation-18 ">

                                <img class="card-image item_thumb"
                                    src="<?php echo "produtos/esfihas/$imagem3" ?>" alt="">
                                <!-- imagem do item -->

                                <div class="card-infos ">

                                    <div class="card-monte">
                                    </div>
                                    <?php
    							$SQL1 = "SELECT * FROM borda";
    
    							$res1 = mysqli_query($conn,$SQL1) or 
    											die("Erro na consulta2");
    							while ($row1 = mysqli_fetch_assoc($res1)){
    								$recheio = $row1['recheio'];
    								$valorborda = $row1['valor'];							
    							?>
                                    <div class="simpleCart_shelfItem">
                                        <div class="chip-adicional "><i
                                                class="icon ion-ios-add-circle-outline"></i>
                                            <!-- adicionar itens adicionais! -->

                                            <div class="item_price price"> <?php echo "R$" . $valorborda ?>
                                            </div>
                                            <a href="javascript:;" class=" single open-full">
                                                <div class=" item_add">
                                                    <div class="item_name "><?php echo $recheio ?> </div>
                                                </div>
                                            </a>

                                        </div>
                                    </div>

                                    <?php } ?>
                                    <!-- Fim Adicional -->
                                    <div class="simpleCart_shelfItem">

                                        <h2 class="card-title item_name"><?php echo $sabor3 ?></h2>
                                        <!-- Nome do item -->
                                        <div class="card-bottom">
                                            <div class="card-author">
                                                <div class=""><?php $ingredientes3 ?></div>
                                                <!-- Descrição do item -->
                                            </div>
                                        </div>
                                        <div class="list list-adicionais">
                                            <!-- Itens Adicionais -->
                                            <ul>
                                                <li class="accordion-item"><a href="#"
                                                        class="item-content item-link">
                                                        <div class="item-inner">
                                                            <div class="item-title">Especiais</div>
                                                        </div>
                                                    </a>
                                                    <div class="accordion-item-content">
                                                        <div class="item-title subtitulo">Selecione o tamanho
                                                        </div>
                                                        <div class="block row padding-bottom">

                                                            <label class="item-checkbox item-content">
                                                                <input id="valor<?php echo $valor3 ?>" name="valormedia<?php echo $valor3 ?>" value="<?php echo $valor3?>" type="checkbox" onchange="(marcaDesmarca(this,<?php echo $valor3 ?>), Soma(<?php echo $valor3?>));" />
                                                                <i
                                                                    class="icon icon-checkbox"><?php echo "Valor R$" . $valor3 ?></i>
                                                            </label>

                                                        </div>
                                                        <div class="item-title subtitulo">Adicionais</div>
                                                        <div class="block row padding-bottom">
                                                            <?php
														$SQL4 = "SELECT * FROM borda";
						
														$res4 = mysqli_query($conn,$SQL4) or 
																		die("Erro na consulta4");
														while ($row4 = mysqli_fetch_assoc($res4)){
															$recheio4 = $row4['recheio'];
															$valorborda4 = str_replace(".",",",$row4['valor']);		
    									                    $id_borda4 = $row4['id_borda'];
															 
														?>
                                                            <label class="item-checkbox item-content">
                                                                <input type="checkbox" name="<?php echo "borda".$id_borda4?>" value="<?php echo $valorborda4?>" onchange="(VerificarTamanhoSelecionado(this, <?php echo $valorborda4?>), Soma(<?php echo $valorborda4?>))" />
                                                                <i
                                                                    class="icon icon-checkbox"><?php echo "R$ " . $valorborda4 . " - " .  $recheio4 ?></i>
                                                            </label>

                                                            <?php } ?>
                                                        </div>
                                                        
                                                        <input type="Hidden" name="acao" value="incluir" >
                                                        <input type="Hidden" name="categoria" value="E" >
                                                        <input type="Hidden" name="img" value="<?php echo $imagem3?>" >
                                                        <input type="Hidden" name="cod" value="<?php echo $id_esfiha?>" >
                                                        <input type="Hidden" name="restaurante" value="<?php echo $restaurante?>" >
                                                        <button type="submit">
                                                                <div class="post-category item_add row">
                                                                     Adicionar
                                                                </div>
                                                            </button>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div><!-- Itens Adicionais -->

                                    </div>
                                </div>

                                <!--		</div>
            <!-- Item Final -->
                            </div>
                            </form>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 