<?php session_start();?>
<?php
if (isset($_SESSION["id"]) == 0){

 header("location: login.php");

} 
?>
<html>
	<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<title>Editar Pizza</title>

        <link rel="stylesheet" href="css/style.css">

	</head>
	<body>       


        <div class="menutop">
        </div>
        
        <div class="conteudo">
			<div class="colunamenu">
                <?php
                    include('submenu.php');
                ?>
	        </div>

			<div class="center"> 
                <h1>Editar Cadastro da Pizza</h1>
		        <br>
		        <table border="0">
		            <form action="edtPizza.php" method="post" enctype="multipart/form-data" name="edtPizza" >
    			    <tr>
    				    <td>Cod da Pizza:
        				<td>
        					<?php 
        					    $tabela = "pizza";
        					    include('conPizza.php');
        					?>
        					<input type='text' style="font-weight:bold" readonly name="id_pizza" maxlength='5' size='25' value='<?php echo $id_pizza; ?>'/>
    			    </tr>
    			    <tr>
    				<td>Sabor:
    				    <td><input type="text" name="sabor" size="60"value = '<?php echo $sabor;?>'  style="text-transform:uppercase"/>
    			    </tr>
    			    <tr>
    				    <td>Ingredientes:
    				    <td><input type="text" name="ingredientes" value = '<?php echo $ingredientes;?>'size="120" />
    			    </tr>
    			    <tr>
    				    <td>Valor da meia:
    				    <td><input type="text" name="valormeia" value = '<?php echo $valormeia;?>'size="14" />
    			    </tr>
    			    <tr>
    				    <td>Valor da pequena:
    				    <td><input type="text" name="valorpequena" value = '<?php echo $valorpequena;?>'size="14" />
    			    </tr>
    			    <tr>
    				    <td>Valor da media:
    				    <td><input type="text" name="valormedia" value = '<?php echo $valormedia;?>'size="14" />
    			    </tr>
    			    <tr>
    				    <td>Valor da grande:
    				    <td><input type="text" name="valorgrande" value = '<?php echo $valorgrande;?>'size="14" />
    			    </tr>
    			    <tr>
				    <td>Restaurante:
				    <td>
				        <select name="id_rest" id="id_rest">
				        <?php
				            include '../complemento/conexao.php';
				            $sql =  "select * from restaurante where id_rest = $id_rest";
				            $rs = mysqli_query($conn,$sql);
				            while($reg = mysqli_fetch_object($rs)):
				        ?>
				            <option value="<?php echo $reg->id_rest ?>"><?php echo $reg->restaurante ?></option>
				        <?php
				            endwhile;
				        ?>
				            
				        <?php
				            include '../complemento/conexao.php';
				            $sql =  "select * from restaurante group by restaurante";
				            $rs = mysqli_query($conn,$sql);
				            while($reg = mysqli_fetch_object($rs)):
				        ?>
				            <option value="<?php echo $reg->id_rest ?>"><?php echo $reg->restaurante ?></option>
				        <?php
				            endwhile;
				        ?>
				        </select>
				    </td>
			    </tr>
    			    <tr>
    			        <td>Imagem:</td>
    			        <td><img src='../produtos/pizzas/<?php echo $img; ?>' style='width:100px;height:100px;'></td>
    			    </tr>
    			    <tr>
    				    <td></td>
    				    <td><input type="file" name="img" value="" multiple/></td>
    			    </tr>
        			<tr>  
        				<td  align="center"><input type="submit" name="Alterar" value="Alterar"/>
        				<td> <input type="reset" name="limpar" value="Limpar Dados"/>
        			</tr>
    		        </form>
		        </table>
	        </div>

	    </div>
	</body>
</html>

