<?php  session_start(); ?>
<html>
<head>
    <title>Login</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<div class="conteudo">
    <div class="login">
        <?php
            if (isset($_SESSION["id"]) == 0){
                include ('comp/formLogin.php');
          }elseif(isset($_GET['logoff'])==1){
               session_destroy();
               header("location: ../index.php");
          }else{ 
               header('location: /index.php'); 
          }?>
    </div>
</div>
</body>
</html>