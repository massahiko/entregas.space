<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags-->
        <meta charset="utf-8">
        <title>bbbbbb</title>
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
    	<meta property="og:title" content="<?php echo $_GET['restaurante'] ?>" />
    	<meta property="og:type" content="website" />
    	<meta property="og:url" content="http://entregas.space/App/" />
    	<meta property="og:image" content="http://entregas.space/App/logo/<?php echo $logo ?>" />
        <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui, viewport-fit=cover">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="mobile-web-app-capable" content="yes">
        <link rel="manifest" href="/manifest.json">
        <link rel="stylesheet" href="css/framework7.ios.min.css">
        <link rel="stylesheet" href="css/ionicons.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="shortcut icon" href="img/icone.png" />
        <style>
            button {
                background-color: Transparent;
                background-repeat:no-repeat;
                border: none;
                cursor:pointer;
                overflow: hidden;
                outline:none;
            }
        </style>
    </head>
  <body class="color-theme-pink">
      <div class="container simpleStore">           
          <div class="simpleStore_container"></div>
          <div class="simpleStore_cart_container"></div>
      </div>
        <!-- App root element -->
    <div id="app">
      <div class="views tabs">
        <!-- Main view -->
        <div id="view-today" class="view view-main tab tab-active">
            <div data-name="home" class="page no-navbar">
              <!-- Scrollable page content -->
              <div class="page-content  infinite-scroll-content" data-infinite-distance="70">
                <div class="ptr-preloader">
                </div>
                <div class="block">
                    <div class="title-container black">
                      <span class="title-date color-pink"><?php echo $_GET['restaurante']?></span>
                      <div class="title-with-link">
                        <h1 class="brand brand-title" ></h1> 
                        <div class="title-with-link">
                          <div class="modo-app">
                              <i id="goFS" class="icon ion-ios-phone-portrait"></i>
                              <h6>Modo App</h6>
                          </div>
                        </div>                  
                      </div>
                    </div>
                    <?php
	                
					include 'complemento/conexao.php';
					$restaurante = $_GET['restaurante'];
					
					$sql =  "select * from restaurante WHERE restaurante = '$restaurante' ";
                    
				    $rs = mysqli_query($conn,$sql)or 
									die("Erro na consulta");
				    while ($row = mysqli_fetch_assoc($rs)){
				        $id_rest = $row['id_rest'];
				    }
					$comandoSQL = "SELECT * FROM pizza  WHERE id_rest = $id_rest";

					$res = mysqli_query($conn,$comandoSQL) or 
									die("Erro na consulta");
					while ($row = mysqli_fetch_assoc($res)){
					    $id_pizza = $row['id_pizza'];
						$sabor = $row['sabor'];
						$ingredientes = $row['ingredientes'];
						$valormeia = $row['valormeia'];
						$valorpequena = $row['valorpequena'];
						$valormedia = $row['valormedia'];
						$valorgrande = $row['valorgrande'];
						$imagem = $row['img'];
					
					?>
                    <!-- Item Inicio -->  
                    <div class="card elevation-18 ">
                      <img class="card-image item_thumb" src=<?php echo "produtos/pizzas/" . $imagem ?> alt="">  <!-- imagem do item -->
                      
                      <div class="card-infos ">
                          
                        <!-- Adicional --> 
                        <div class="card-monte">                          
                            <div class="" >Monte sua pizza:</div>
                          </div>	
                            <?php
								$SQL1 = "SELECT * FROM borda";

								$res1 = mysqli_query($conn,$SQL1) or 
												die("Erro na consulta");
								while ($row1 = mysqli_fetch_assoc($res1)){
									$recheio = $row1['recheio'];
									$valorborda = $row1['valor'];							
								?>						  
                          <div class="simpleCart_shelfItem">
                            <div class="chip-adicional " ><i class="icon ion-ios-add-circle-outline"></i> <!-- adicionar itens adicionais! -->
								
                                <div  class="item_price price"> <?php echo "R$" . $valorborda ?>  </div>
                                <a href="javascript:;"  class=" single open-full">
                                    <div class=" item_add"><div  class="item_name "><?php echo $recheio ?>   </div></div>  
                                </a>
							</div>							  
                          </div>
						  
								<?php } ?>
                          <!-- Fim Adicional -->
                        <div class="simpleCart_shelfItem">
                        <div class="chip-pizza">
                          <img  src="img/pizza.png" alt="" class="size-pizza">
                          <div class="size-pizza-text  color-white">
                           <select   class=" item_description" > <!-- Select com itens para escolher metade da pizza cadastrada! -->
                           <optgroup label="Cada pizza tem sua combinação exclusiva!"></optgroup>
                           <optgroup label="Caso não encotre o segundo sabor desejado, monte sua pizza apartir desse sabor!"></optgroup>
                            <option selected disabled hidden>Pizza meia a meia</option>
                            <?php
                                $sql2 =  "select * from pizza where id_pizza <> $id_pizza  and id_rest = $id_rest group by sabor";
        			            $rs2 = mysqli_query($conn,$sql2);
        			            while($reg2 = mysqli_fetch_object($rs2)):
        			        ?>
        			            <option value="<?php echo $reg2->id_pizza ?>"><?php echo $reg2->sabor ?></option>
        			        <?php
        			            endwhile;
        			        ?>
                            
                          </select>
                        </div>
                        </div>
                        <div class="chip color-white" ><i class="icon ion-ios-resize"></i>
                          <select  class="item_price "  > <!-- escolher varios tamanhos, de pizza! cada tamanho com seu devido valor! -->
                            <option selected disabled hidden > Selecione o Tamanho</option>
                            <option value="40" > <?php echo "Pequena R$" . $valorpequena ?>  </option>
                            <option value="48">  <?php echo "Média R$" . $valormedia ?>  </option>
                            <option value="55" > <?php echo "Grande R$" . $valorgrande ?>  </option>  
                          </select> 
                      </div>
                        <h2 class="card-title item_name"><?php echo $sabor ?> </h2> <!-- Nome do item -->
                        <div class="card-bottom">
                          <div class="card-author">                          
                            <div class="" ><?php echo $ingredientes ?> </div>  <!-- Descrição do item -->
                          </div>
                        </div>
                        
                        <a href="javascript:;"  class=" single open-full">
                          <div class="post-category add item_add"> <i class="icon ion-md-basket"></i>Adicionar</div>  
                        </a>
                     </div>
                    </div>
                    </div>
					<?php } ?>
                    <!-- Item Final --> 
                </div>
              </div>
            </div>
          </div>
        <!-- Tela2 View -->
        <div id="view-discover" class="view tab">
            <div data-name="discover" class="page no-navbar">
              <!-- Scrollable page content -->
              <div class="page-content">
                    <?php
                    include 'complemento/conexao.php';
					$restaurante = $_GET['restaurante'];
					
					$sql =  "select * from restaurante WHERE restaurante = '$restaurante' ";
                    
				    $rs = mysqli_query($conn,$sql)or 
									die("Erro na consulta");
				    while ($row = mysqli_fetch_assoc($rs)){
				        $id_rest = $row['id_rest'];
				    }
				    ?>
                  <div class="block">
                      <div class="title-container black">
                        <span class="title-date color-pink"><?php echo $restaurante ?></span>
                        <div class="title-with-link">
                          <h1 class="brand brand-title" ></h1>                       
                        </div>
                      </div>
                    </div>
                    
                <div id="discover-swiper" class="swiper-container">
                  <div class="swiper-wrapper">
                     <?php
	                
					
					$comandoSQL = "SELECT * FROM esfiha  WHERE id_rest = $id_rest";

					$res = mysqli_query($conn,$comandoSQL) or 
									die("Erro na consulta");
					while ($row = mysqli_fetch_assoc($res)){
					    $id_esfiha = $row['id_esfiha'];
						$sabor = $row['sabor'];
						$ingredientes = $row['ingredientes'];
						$valor = $row['valor'];
						$imagem = $row['img'];
					
					?>


                    <div class="swiper-slide  ">
                      
                        <div class="card ">
                          <img class="card-image" src="produtos/esfihas/carne.jpg" alt="">
                          <div class="card-infos2">
                            <h2 class="card-title slider-title"><?php echo $sabor ?></h2>
                            <div class="author-description slider-subtitle"><?php echo $ingredientes ?>
                                </div>
                                <div class="author-block simpleCart_shelfItem">                               
                                  <div class="author-infos row">
                                    <div class="item_price author-price"><?php echo "R$".$valor ?></div>
                                    <div class="author-name item_name">1x Esfiha Sabor Carne </div>                                    
                                    <a href="javascript:;"  class=" single open-full">
                                      <div class="post-category2 add-slide item_add"> Adicionar</div>  
                                    </a>
                                  </div>                                
                              </div>
                              
                            </div>
                        </div>
                      
                    </div>
                    
					<?php } ?>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- Categories View -->
        <div id="view-categories" class="view tab">
            <div data-name="categories" class="page no-navbar">
              <!-- Scrollable page content -->
              <div class="page-content">
                  <div class="block">
                      <div class="title-container black">
                          
                        <span class="title-date color-pink">entregas.space/</span>
                        <div class="title-with-link">
                          <h1 class="brand brand-title" ></h1> 
                                             
                        </div>
                      </div>
                    </div>
                  <div class="block">
                      <div class="two-columns-cards">
                        <!-- Inicio View -->
                        <a href="#">
                          <div class="card simpleCart_shelfItem">
                            <img class="card-image item_thumb" src="produtos/bebidas/cocamini.jpg" alt="">
                            <div class="card-infos">
                              <h2 class="card-title-drink item_name">Coca Cola Lata Mini</h2>
                              <div class="row">
                              <div class="add-price item_price">R$2.50</div>
                              <div href="javascript:;" class="add-slide2 item_add open-full">adicionar</div>
                            </div>
                            </div>
                          </div>
                        </a>
                        <!-- Fim View -->
                 
                       </div>
                    </div>
                
              </div>
            </div>
          </div>

  

      <div class="login-screen">
                    <div class="view">
                      <div class="page">
                        <div class="page-content login-screen-content">
                            <a href="#"  class="link login-screen-close close-button">
                                <img src="img/close.svg" alt="Close">
                              </a>
                          <div class="login-screen-title">Seu Pedido</div>
                          <div class="block search-container simpleStore_cart" >
                            <!-- Cart View -->
          
                            <div id="cart" >
                                <div  id='container'>
                                    <ul class="list media-list post-list block">
                                        <li>
                                            <div class="item-content">
                                              <div class="item-media"><img src="img/domcavati.jpg" alt=""></div>
                                              <div class="item-inner">
                                                <div class="row col">
                                                  <div class="item-subtitle col-45">Moda da Casa </div>
                                                  <div class="item-subtitle  col-45">(Grande)</div>
                                                  <div class="col-20 item-delete"><i class="icon ion-md-trash"></i></div>
                                                </div>
                                                <div class=" bottom-subtitle ">Meia Calabresa</div>
                                                <div class="row col">
                                                  <div class="item-subtitle bottom-subtitle ">1x <div class="padding-10">R$45,00</div></div>
                                                  
                                                </div>
                                                <div class="item-subtitle bottom-subtitle"><i class="icon ion-md-add"></i> <div class="padding-right-10">R$5</div> Borda de Catupiry</div>
                                                <div class="item-subtitle bottom-subtitle"><i class="icon ion-md-add"></i> <div class="padding-right-10">R$5</div> Borda de Catupiry</div>
                                                <div class="item-subtitle bottom-subtitle">Sub-Total: <div class="padding-10">R$55,00</div> </div>
                                                
                                              </div>
                                            </div>
                                          
                                        </li>
                                        
                                        </ul>
                                    <div class="data-table row">
                                      
                                    <a href="javascript:;" class="simpleCart_empty button color-gray u-pull-left">Limpar Carrinho <i class="fa fa-trash-o"></i></a>
                                    <a href="javascript:;" class="button button-primary  u-pull-right">Total: <i class="fa fa-arrow-right simpleCart_grandTotal"></i></a>
                                  </div>
                                    <div class="list no-hairlines custom-form contact-form">
                                        <form class="list " >
                                        <ul >
                                            <div class="row pay-top-2"> 
                                                <div class="col-60">  <h5 >Formas de Pagamentos</h5></div>    
                                                <div class="col-40"> <h5 >Total: <i class="fa fa-arrow-right simpleCart_grandTotal"></i></h5></div>
                                                <div class="col-65  pay-top">  <li class="item-content item-input">                                         
                                                    <div class="item-inner">
                                                        <div class="item-input-wrap">
                                                          <input type="number" placeholder="Dinheiro, troco para?" >
                                                          <span class="input-clear-button"></span>
                                                        </div>
                                                      </div>
                                                </div>
                                                <div class="col-35 row  pay-top">
                                                    <h5>Cartão</h5>
                                                    <label class="toggle toggle-init color-pink">
                                                      <input type="checkbox">
                                                      <span class="toggle-icon"></span>
                                                    </label>
                                                  </div>
                                            </div>
                                            
                                            <li class="item-content item-input">
                                                <div class="item-inner">
                                                  <div class="item-input-wrap">
                                                    <textarea class="resizable" placeholder="Adicione uma observação para seu pedido!"></textarea>
                                                    <span class="input-clear-button"></span>
                                                  </div>
                                                </div>
                                              </li>
                                          
                                          <h5>Endereço de Entrega</h5>
                                          <form class="list form-store-data store-data" id="my-form">
                                          <li class="item-content item-input">
                                            <div class="item-inner">
                                              <div class="item-input-wrap">
                                                <input type="text" name="rua" placeholder="Rua">
                                                <span class="input-clear-button"></span>
                                              </div>
                                            </div>
                                            <div class="item-inner">
                                                <div class="item-input-wrap">
                                                  <input type="number" name="numero" placeholder="Número">
                                                  <span class="input-clear-button"></span>
                                                </div>
                                              </div>
                                          </li>
                                          <li class="item-content item-input">
                                              <div class="item-inner">
                                                  <div class="item-input-wrap input-dropdown-wrap ">
                                                    <select name="cidade">
                                                      <option value="01">Dom Cavati-MG</option>
                                                      
                                                    </select>
                                                  </div>
                                                </div>
                                            <div class="item-inner">
                                              <div class="item-input-wrap">
                                                <input type="text" name="bairro" placeholder="Bairro">
                                                <span class="input-clear-button"></span>
                                              </div>
                                            </div>
                                          </li>
                                        </form>
                                        </ul>
                                        </form>
                                      </div>  
                                </div>
                            </div>
                            <a  onclick='screenshot();' id="button1" class="big-button promo-banner-2 button button-fill link open-preloader-indicator" href="#" data-popup=".popup-about">
                              <i class="icon ion-ios-send "></i>Confirmar Pedido
                            </a>  
                            <div id="div_links" class="w-button"></div>
                           
                    </div>
                        </div>
                      </div>
                    </div>
      </div>



       
      <!-- <a  onclick='screenshot();' id="button1" class="big-button promo-banner-2 button button-fill link popup-open " href="#" data-popup=".popup-about">
                              <i class="icon ion-ios-send "></i>Confirmar Pedido
           </a>  -->  
 <!-- Checkout View 
          <div class="popup popup-about">
                <div class="block">
                  <div class="w-info"><h5>Totalmente integrado ao</h5></div>
                  <div class="w-title"><h5>WhatsApp</h5></div>
                  
          <p id='Label2' style='color:red;'> </p>
                              
                  <a href="#" class="link popup-close close-button" onClick="history.go(0)"><img src="img/close.svg" alt="Close"></a>
                    <div class="w-text">Integramos nosso sistema ao aplicativo mais utilizado no mundo para agilizar o seu pedido!</div>
                    
                    <div id="div_links" class="w-button"><div class="loading">Aguarde enquanto preparamos o seu envio!</div></div>
                      <div class="w-img"> 
                          <img class="w-img" src="img/back.png" alt="">                      
                      </div>
                      <div class="w-version"><h5>ENTREGAS.SPACE &nbsp&nbsp V-0.1@beta</h5></div>
                </div>
          </div>
        -->

        <!-- Toolbar -->
        <div class="toolbar tabbar tabbar-labels">
            <div class="toolbar-inner">
              <a href="#view-today" class="tab-link tab-link-active tab-icon">
                  <i class="icon ion-ios-pizza"></i> 
                <span class="tabbar-label">Pizzas</span>
              </a>
              <a href="#view-discover" class="tab-link">
                <i class="icon ion-ios-radio-button-off"></i>
                <span class="tabbar-label">Esfihas</span>
              </a>
              <a href="#view-categories" class="tab-link ">
                <i class="icon ion-ios-cafe"></i>
                <span class="tabbar-label">Bebidas</span>
              </a>
              <a href="#" class="tab-link">
              </a>
              <a href="#" class="tab-link login-screen-open" data-login-screen=".login-screen">
                <i class="icon ion-md-basket"><span class="badge badge-position color-pink simpleCart_quantity"></span></i>              
                <span class="tabbar-label">Pedido</span>
              </a>
            </div>
          </div>
        </div>
    </div>





    <!-- Scripts

     
–––––––––––––––––––––––––––––––––––––––––––––––––– -->



<script src="js/framework7.min.js"></script>
<script src="js/app.js"></script>


<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="js/simpleCart.min.js"></script>
<script src="js/simpleStore.js"></script>
<script src="js/whatsapp.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.1/dist/html2canvas.min.js"></script>

<script src="js/config.js"></script>
<script>
    if ('serviceWorker' in navigator) {
       console.log("Will the service worker register?");
       navigator.serviceWorker.register('service-worker.js')
         .then(function(reg){
           console.log("Yes, it did.");
        }).catch(function(err) {
           console.log("No it didn't. This happened:", err)
       });
    }
   </script>
   <script>
    var goFS = document.getElementById("goFS");
    goFS.addEventListener("click", function() {
        document.body.requestFullscreen();
    }, false);
  </script>
  </body>
</html>
