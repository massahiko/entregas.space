<?php
         // Inicializando a sessão...
         session_start();

        // Destruindo a sessão
        session_destroy();

        // Redireccionar o utilizador depois da destruição da sessão...
        header("Location: ../index.php");
?>