<?php session_start();?>
<?php
if (isset($_SESSION["id"]) == 0){

 header("location: login.php");

} 
?>
<html>
<head>
    <title>Cadastro de Bebida</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>


    <div class="conteudo">
		<div class="colunamenu">
            <?php
                include('submenu.php');
            ?>
	    </div>
		<div class="center"> 
            <h1>Cadastro de Bebida</h1>
		    <br>
		    <table border="0">
		        <form action="cadBebida.php" method="post" enctype="multipart/form-data"name="cadBebida">

			    <tr>
				    <td>Nome:
				    <td><input type="text" name="nome" size="60"/>
			    </tr>
			    <tr>
				    <td>Detalhes:
				    <td><input type="text" name="detalhes" size="120"/>
			    </tr>
			    <tr>
				    <td>Valor:
				    <td><input type="text" name="valor" size="14" />
			    </tr>
			    <tr>
				    <td>Restaurante:
				    <td>
				        <select name="id_rest" id="id_rest">
				            
				        <?php
				            include '../complemento/conexao.php';
				            $sql =  "select * from restaurante group by restaurante";
				            $rs = mysqli_query($conn,$sql);
				            while($reg = mysqli_fetch_object($rs)):
				        ?>
				            <option value="<?php echo $reg->id_rest ?>"><?php echo $reg->restaurante ?></option>
				        <?php
				            endwhile;
				        ?>
				        </select>
				    </td>
			    </tr>
			    <tr>
				    <td>Imagem:
				    <td><input type="file" name="img" value="" /></td>
			    </tr>
    			<tr>  
    				<td  align="center"><input type="submit" name="salvar" value="Salvar"/>
    				<td> <input type="reset" name="limpar" value="Limpar Dados"/>
    			</tr>
		        </form>
		    </table>
        </div>
	</div>
</body>
</html>