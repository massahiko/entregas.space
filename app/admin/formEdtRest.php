<?php session_start();?>
<?php
if (isset($_SESSION["id"]) == 0){

 header("location: login.php");

} 
?>
<html>
	<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<title>Editar Restaurante</title>

        <link rel="stylesheet" href="css/style.css">

	</head>
	<body>       


        <div class="menutop">
        </div>
        
        <div class="conteudo">
			<div class="colunamenu">
                <?php
                    include('submenu.php');
                ?>
	        </div>

			<div class="center"> 
                <h1>Editar Cadastro da Restaurante</h1>
		        <br>
		        <table border="0">
		            <form action="edtRest.php" method="post" enctype="multipart/form-data" name="edtRest" >
    			    <tr>
    				    <td>Cod da Restaurante:
        				<td>
        					<?php 
        					    $tabela = "Restaurante";
        					    include('conRest.php');
        					?>
        					<input type='text' style="font-weight:bold" readonly name="id_rest" maxlength='5' size='25' value='<?php echo $id_rest; ?>'/>
        				</td>
    			    </tr>
    			    <tr>
    				<td>Restaurante:</td>
    				    <td><input type="text" name="restaurante" size="60"value = '<?php echo $restaurante;?>' /></td>
    			    </tr>
    			    <tr>
    				    <td>Whatsaap:</td>
    				    <td><input type="text" name="whatsaap" value = '<?php echo $whatsaap;?>'size="120" /></td>
    			    </tr>
    			    <tr>
    				    <td>Responsavel:</td>
    				    <td><input type="text" name="responsavel" value = '<?php echo $responsavel;?>'size="14" /></td>
    			    </tr>
    			    <tr>
    			        <td>Cidade:</td> 
    			        <td>
        			        <select name="cidade">
                                <option value="01"><?php echo $cidade ?></option>
                                <?php
        				            include '../complemento/conexao.php';
        				            $sql =  "select * from cidade group by descricao";
        				            $rs = mysqli_query($conn,$sql);
        				            while($reg = mysqli_fetch_object($rs)):
        				        ?>
        				            <option value="<?php echo $reg->descricao."-".$reg->uf ?>"><?php echo $reg->descricao."-".$reg->uf ?></option>
        				        <?php
        				            endwhile;
        				        ?>            
                            </select>
                        </td>
                    </tr>
    			    <tr>
    			        <td>Logo:</td>
    			        <td><img src='../logo/<?php echo $logo; ?>' style='width:100px;height:100px;'></td>
    			    </tr>
    			    <tr>
    				    <td></td>
    				    <td><input type="file" name="logo" value="" multiple/></td>
    			    </tr>    			    
        			<tr>  
        				<td  align="center"><input type="submit" name="Alterar" value="Alterar"/></td>
        				<td> <input type="reset" name="limpar" value="Limpar Dados"/></td>
        			</tr>
    		        </form>
		        </table>
	        </div>

	    </div>
	</body>
</html>

