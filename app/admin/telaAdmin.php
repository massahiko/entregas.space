<?php session_start();?>
<?php
if (isset($_SESSION["id"]) == 0){

 header("location: login.php");

} 

?>
<html>
	<head>
		<title>Administrador</title>
	    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="stylesheet" href="css/style.css">

    </head>
	<body>

        <div class="conteudo">
    		<div class="colunamenu">
                <?php
                    include('comp/submenu.php');
                ?>
    		</div>
    		<div class="center"> 
                <h2>Lista de Administrador</h2>
                <br/>
    			<table border="1" cellspacing="0"> 
    			    <form action="formCadAdmin.php" method="post" enctype="multipart/form-data">
                    <tr>
    	                <td  align="left" colspan="14"><input type="submit" name="Cadastrar" value="Cadastrar Novo Administrador"/>
    
                    </tr>
    				<tr>
    					<th>Cod Administrador</th>
                        <th>Nome</th>

                        <th></th>
                        <th></th>
    				</tr>
    
                    <?php
    
                        include '../complemento/conexao.php';
                        
                    	$SQL = "  SELECT *";
                    	$SQL .= " FROM admin";
                    	$SQL .= " ORDER BY nome";
                    	$res = mysqli_query($conn,$SQL) or 
                            die("Erro na consulta");
                        $linha = mysqli_num_rows($conn);
                        
                        while ($row = mysqli_fetch_assoc($res)){
                            $id =$row['id_admin'];
                            $nome = $row['nome'];
                            
                            echo "<tr>";
                                echo "<td>" . $id . "</td>";
                                echo "<td>" . $nome . "</td>";

                                echo "<td><a href='formEdtAdmin.php?id_admin=$id'><img src=\"img/editar.ico \"></a></td>";
                                echo "<td><a href='delAdmin.php?id_admin= $id'><img src=\"img/excluir1.png \"></a></td>";
                            echo "</tr>";
                        }?>						
    
                    </form>
                </table>   
    		</div>
		</div>
	</body>
</html>

