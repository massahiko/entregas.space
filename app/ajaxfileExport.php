<?php
session_start();
include 'complemento/conexao.php';
/*
 * Gerar um arquivo .txt para imprimir na impressora Bematech MP-20 MI
 */

$n_colunas = 29; // 29 colunas por linha

/**
 * Adiciona a quantidade necessaria de espaços no inicio
 * da string informada para deixa-la centralizada na tela
 *
 * @global int $n_colunas Numero maximo de caracteres aceitos
 * @param string $info String a ser centralizada
 * @return string
 */
function centraliza($info)
{
    global $n_colunas;

    $aux = strlen($info);

    if ($aux < $n_colunas) {
// calcula quantos espaços devem ser adicionados
        // antes da string para deixa-la centralizada
        $espacos = floor(($n_colunas - $aux) / 2);

        $espaco = '';
        for ($i = 0; $i < $espacos; $i++) {
            $espaco .= ' ';
        }

// retorna a string com os espaços necessários para centraliza-la
        return $espaco . $info;

    } else {
// se for maior ou igual ao número de colunas
        // retorna a string cortada com o número máximo de colunas.
        return substr($info, 0, $n_colunas);
    }

}

/**
 * Adiciona a quantidade de espaços informados na String
 * passada na possição informada.
 *
 * Se a string informada for maior que a quantidade de posições
 * informada, então corta a string para ela ter a quantidade
 * de caracteres exata das posições.
 *
 * @param string $string String a ter os espaços adicionados.
 * @param int $posicoes Qtde de posições da coluna
 * @param string $onde Onde será adicionar os espaços. I (inicio) ou F (final).
 * @return string
 */
function addEspacos($string, $posicoes, $onde)
{

    $aux = strlen($string);

    if ($aux >= $posicoes) {
        return substr($string, 0, $posicoes);
    }

    $dif = $posicoes - $aux;

    $espacos = '';

    for ($i = 0; $i < $dif; $i++) {
        $espacos .= ' ';
    }

    if ($onde === 'I') {
        return $espacos . $string;
    } else {
        return $string . $espacos;
    }

}

$restaurante = $_POST['restaurante'];
$nome = $_POST['nome'];
$telefoneCliente = $_POST['telefoneCliente'];
$rua = $_POST['rua'];
$numero = $_POST['numero'];
$cidade = $_POST['cidade'];
$bairro = $_POST['bairro'];
$totalFinal = Valor($_POST['totalFinal']);
$troco = number_format($_POST['totalTroco'], 2);
$totalTroco = number_format(number_format($troco, 2) - number_format($totalFinal,2),2);
if ($totalTroco < 0){
   $totalTroco =  number_format(0,2);
}
$cartao = $_POST['cartao'];
$observacao = $_POST['observacao'];
$telefone = $_POST['telefone'];
$valorTaxa = Valor($_POST['totalTaxa']);
$subTotal = number_format(number_format($totalFinal, 2) - number_format($valorTaxa,2),2);

        
$txt_cabecalho = array();
$txt_itens = array();
$txt_obs = array();
$txt_valor_total = '';
$txt_rodape = array();


$txt_cabecalho[] = $restaurante;

$txt_cabecalho[] = 'Obrigado pela preferência';

$txt_cabecalho[] = str_repeat("-", 30);

$tot_itens = 0;

$sql_meu_carrinho = "SELECT id, nome, preco, qtd as qtd, sessao, img, categoria, tamanho  FROM tbl_carrinho
                     WHERE  sessao = '" . session_id() . "'
                     ORDER BY nome ASC";


$exec_meu_carrinho = mysqli_query($conn, $sql_meu_carrinho) or die(mysql_error());
$qtd_meu_carrinho = mysqli_num_rows($exec_meu_carrinho);

if ($qtd_meu_carrinho > 0) 
{
    while ($row_rs_produto_carrinho = mysqli_fetch_assoc($exec_meu_carrinho)) 
    {
        if (strlen($row_rs_produto_carrinho['nome']) > 20){
            $txt_itens[] = array(substr($row_rs_produto_carrinho['nome'],0,20),'');
            $txt_itens[] = array(substr($row_rs_produto_carrinho['nome'],21),'R$'.$row_rs_produto_carrinho['preco']);
        }
        else{
            $txt_itens[] = array($row_rs_produto_carrinho['nome'],'R$'.$row_rs_produto_carrinho['preco']);
        }
        
        //$txt_itens[] = '         | '.$row_rs_produto_carrinho['qtd'].'   '.$row_rs_produto_carrinho['preco'].'  '.number_format($row_rs_produto_carrinho['qtd']*$row_rs_produto_carrinho['preco'] ,2);
        $tot_itens = $row_rs_produto_carrinho['preco'];
        $id = $row_rs_produto_carrinho['id'];

        $sql_meu_carrinho_adicional = "SELECT bor.*, adc.* FROM tbl_carrinho_adicionais as adc,borda as bor WHERE bor.id_borda = adc.id_borda and id_carrinho = $id and  sessao = '" . session_id() . "'";


        $exec_meu_carrinho_adicional = mysqli_query($conn, $sql_meu_carrinho_adicional) or die(mysql_error());
        $qtd_meu_carrinho_adicional = mysqli_num_rows($exec_meu_carrinho_adicional);

        if ($qtd_meu_carrinho_adicional > 0)
        {
            while ($row_rs_produto_carrinho_adicional = mysqli_fetch_assoc($exec_meu_carrinho_adicional)) 
            {
                if (strlen('   + '.$row_rs_produto_carrinho_adicional['recheio']) > 20){
                    $txt_itens[] = array(substr('   + '.$row_rs_produto_carrinho_adicional['recheio'],0,20),'');
                    $txt_itens[] = array(substr('     '.$row_rs_produto_carrinho_adicional['recheio'],21),'R$'.$row_rs_produto_carrinho_adicional['valor']);
                }
                else{
                    $txt_itens[] = array('   + '.$row_rs_produto_carrinho_adicional['recheio'], 'R$'.$row_rs_produto_carrinho_adicional['valor']);
                }
                
                $tot_itens += $row_rs_produto_carrinho_adicional['valor'];
            }
        }
        //$txt_itens[] = array('','Sub. Total: R$'.$tot_itens);
        $txt_itens[] = array(str_repeat(" ", 10).'Sub-Total:','R$'.number_format($tot_itens,2));
        $txt_itens[] = array(str_repeat("-", 21), str_repeat("-", 9));
    }
}

$txt_itens[] = array(str_repeat(" ", 10).'Sub-Total:','R$'.$subTotal);
$txt_itens[] = array(str_repeat(" ", 4).'Taxa de Entrega:','R$'.$valorTaxa);
$txt_itens[] = array(str_repeat(" ", 6).'Total a Pagar:','R$'.$totalFinal);

$formaPagamento = "";
if ($cartao == "true"){
    $formaPagamento = "Cartão";
}else{
    $formaPagamento = "Dinheiro";
}
$txt_obs[] = 'OBSERVAÇÕES: '.$observacao;
$txt_obs[] = '------------------------------';
$txt_itens[] = array(str_repeat("-", 21), str_repeat("-", 9));
$txt_itens[] = array(str_repeat(" ", 1).'Forma de Pagamento:',$formaPagamento);
$txt_itens[] = array(str_repeat(" ", 9).'Troco para:','R$'.$troco);
$txt_itens[] = array(str_repeat(" ", 3).'Diferença Troco:',' R$'.$totalTroco);
$txt_itens[] = array(str_repeat("-", 21), str_repeat("-", 9));
$txt_rodape[] = 'Dados para Entrega:';
$txt_rodape[] = 'Nome: '.$nome;
$txt_rodape[] = 'Telefone: '.$telefoneCliente;
$txt_rodape[] = 'Rua: '.$rua;
$txt_rodape[] = 'Número: '.$numero;
$txt_rodape[] = 'Cidade: '.$cidade;
$txt_rodape[] = 'Bairro: '.$bairro;


// centraliza todas as posições do array $txt_cabecalho
$cabecalho = array_map("centraliza", $txt_cabecalho);

/* para cada linha de item (array) existente no array $txt_itens,
 * adiciona cada posição da linha em um novo array $itens
 * fazendo a formatação dos espaçamentos entre cada coluna
 * da linha através da função "addEspacos"
 */

foreach ($txt_itens as $item) {

/*
 * nome => máximo de 15 colunas
 * valor => máximo de 15 colunas
 * $itens[] = 'nome valor'
 */

    $itens[] = addEspacos($item[0], 20, 'F')
    . addEspacos($item[1], 9, 'I');
}

/* concatena o cabelhaço, os itens, o sub-total e rodapé
 * adicionando uma quebra de linha "\r\n" ao final de cada
 * item dos arrays $cabecalho, $itens, $txt_rodape
 */
$txt = implode("\r\n", $cabecalho)
. "\r\n"
. implode("\r\n", $txt_obs)
. "\r\n"
. implode("\r\n", $itens)
. "\r\n"
. implode("\r\n", $txt_rodape);

// caminho e nome onde o TXT será criado no servidor
$fileName = uniqid().'.txt';
$file = "pedidos/".$fileName;

// cria o arquivo
$_file = fopen($file, "w");
fwrite($_file, "\xEF\xBB\xBF");
fwrite($_file, $txt);
fclose($_file);

header("Pragma: public");
// Força o header para salvar o arquivo
header("Content-type: application/save");
header("Content-type: txt/html; charset=utf-8");
header("X-Download-Options: noopen "); // For IE8
header("X-Content-Type-Options: nosniff"); // For IE8
// Pré define o nome do arquivo
header("Content-Disposition: attachment; filename=imprimir.txt");
header("Expires: 0");
header("Pragma: no-cache");

$restParam = 'restaurante='.$restaurante;
$fileParam = "pedido=".str_replace('.txt','',$fileName);

LimparCarrinho();

echo 'pedido.php?'.$fileParam."&".$restParam;
exit;

function Valor($valor) {
    if(strpos("[".$valor."]", "."))
    {
        $valor = str_replace('.','', $valor);
        $valor = str_replace(',','.', $valor);
    }
    elseif(strpos("[".$valor."]", ","))
    {
        $valor = str_replace(',','.', $valor);   
    }
    else
    {
        $valor = $valor.".00";
    }
    return $valor;
}

function LimparCarrinho(){
    include 'complemento/conexao.php';
    
    // Verificamos se o produto referente ao $cod  está no carrinho para o session id correnpondente
    $query_rs_car = "SELECT * FROM tbl_carrinho WHERE  sessao = '" . session_id() . "'";
    $rs_car = mysqli_query($conn, $query_rs_car) or die(mysqli_error());
    $row_rs_carrinho = mysqli_fetch_array($rs_car);
    $totalRows_rs_car = mysqli_num_rows($rs_car);

    // Se encontrarmos o registro, limpamos o carrinho
    if ($totalRows_rs_car > 0) {
        $sql_carrinho_excluir = "DELETE FROM tbl_carrinho WHERE sessao = '" . session_id() . "'";
        $exec_carrinho_excluir = mysqli_query($conn, $sql_carrinho_excluir) or die(mysqli_error());
        $sql_carrinho_adicionais_excluir = "DELETE FROM tbl_carrinho_adicionais WHERE sessao = '" . session_id() . "'";
        $exec_carrinho_adicionais_excluir = mysqli_query($conn, $sql_carrinho_adicionais_excluir) or die(mysqli_error());
    }
}


?>