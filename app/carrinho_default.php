<?php
$restaurante = $_GET['restaurante'];
?>

<!DOCTYPE html>
<html>
  <head>
    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui, viewport-fit=cover">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <title>Pedidos</title>
    <link rel="stylesheet" href="css/framework7.ios.min.css">
    <link rel="stylesheet" href="css/ionicons.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  
  <body class="color-theme-pink">
    <div class="page single single-1 no-navbar" data-name="single">    
      <div class="page-content">
        <div class="block article">
          <img class=" img-delivery" src="img/delivery.png" alt="">  
      
          <p class="info-delivery">Escolha seus itens</p>   
          
          </div>
        </div>
      </div>
      <div class="block article">
      <form action="index.php" method="GET">
        <input type="Hidden" name="restaurante" value="<?php echo $restaurante ?>" >
        <button type="submit"> 
          <div class="big-button button button-fill link btn-delivery"><i class="icon ion-ios-send"></i>Voltar para o cardápio!</div>
        </button>
      </form>
    </div>
  

    <script src="js/framework7.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>