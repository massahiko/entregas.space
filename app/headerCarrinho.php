<!DOCTYPE html>
<html>
    <base href="../app/">
    <head>
        <!-- Required meta tags-->
        <meta charset="utf-8">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <meta property="og:title" content="<?php echo $_GET['restaurante'] ?>" />
    	<meta property="og:type" content="website" />
    	<meta property="og:url" content="https://entregas.space/app/" />
    	<meta property="og:image" content="https://entregas.space/app/logo/<?php echo $logo ?>" />
        <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui, viewport-fit=cover">
        <meta name="apple-mobile-web-app-capable" content="yes"> 
        <meta name="mobile-web-app-capable" content="yes">
        <link rel="manifest" href="/manifest.json">
        <link rel="stylesheet" href="css/framework7.ios.min.css">
        <link rel="stylesheet" href="css/ionicons.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="shortcut icon" href="img/icone.png" />
        <style>
            button {
                background-color: Transparent;
                background-repeat:no-repeat;
                border: none;
                cursor:pointer;
                overflow: hidden;
                outline:none;
            }
        </style>
    </head>