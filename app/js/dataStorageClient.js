const _TABELA_CLIENTE = "dbcliente";

$(document).ready(function()
{
    UpdateDataCliente();
});

function GravarDadosCliente() 
{
    if (document.querySelector("#nome") == null)
    {
        return;
    }

    let data = GetDataCliente();

    if (data == null) 
    {
        var banco = JSON.stringify({
                nome: document.querySelector("#nome").value,
                telefone: document.querySelector("#telefone").value,
                rua: document.querySelector("#rua").value,
                numero: document.querySelector("#numero").value,
                cidade: document.querySelector("#cidade").value,
                bairro: document.querySelector("#bairro").value
            });
        
        localStorage.setItem(_TABELA_CLIENTE, banco);
        return;
    }

    data = JSON.stringify({
        nome: document.querySelector("#nome").value,
        telefone: document.querySelector("#telefone").value,
        rua: document.querySelector("#rua").value,
        numero: document.querySelector("#numero").value,
        cidade: document.querySelector("#cidade").value,
        bairro: document.querySelector("#bairro").value
    });
    
    localStorage.setItem(_TABELA_CLIENTE, data);
    return true;
}

function GetDataCliente()
{
    let data = localStorage.getItem(_TABELA_CLIENTE);
    return JSON.parse(data);
}

function UpdateDataCliente()
{
    var banco = GetDataCliente();
    if (banco == null)
    {
        GravarDadosCliente();
        return;
    }

    if (!banco.nome) 
    {
        return;    
    }

    $("#nome").val(banco.nome);
    $("#telefoneCliente").val(banco.telefone);
    $("#rua").val(banco.rua);
    $("#numero").val(banco.numero);
    $("#cidade").val(banco.cidade);
    $("#bairro").val(banco.bairro);

    var select = document.getElementById('bairro');
    atualizaTotal(select.options[select.selectedIndex].id);
}

