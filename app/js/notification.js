
// Create toast with icon
var toastIcon = app.toast.create({
    icon: app.theme === 'ios' ? '<i class="ion-md-basket"></i>' : '<i class=" ion-md-basket"></i>',
    text: 'Item adicionado ao pedido!',
    position: 'top',
    closeTimeout: 1200,
  });
  // Open toasts
  $$('.open-toast-bottom').on('click', function () {
    toastBottom.open();
  });
  $$('.open-toast-top').on('click', function () {
    toastTop.open();
  });
  $$('.open-toast-center').on('click', function () {
    toastCenter.open();
  });
  $$('.open-toast-icon').on('click', function () {
    toastIcon.open();
  });
  $$('.open-toast-large').on('click', function () {
    toastLargeMessage.open();
  });
  $$('.open-toast-button').on('click', function () {
    toastWithButton.open();
  });
  $$('.open-toast-custom-button').on('click', function () {
    toastWithCustomButton.open();
  });
  $$('.open-toast-callback').on('click', function () {
    toastWithCallback.open();
  });

// Create Popup with swipe to close
var swipeToClosePopup = app.popup.create({
    el: '.popup-swipe-to-close',
    swipeToClose: true,
  });