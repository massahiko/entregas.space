function ValidarValores(frm){
    
    return false;
}

function marcaDesmarca(obj,id) {
    
    expandCard("elevation" + id);
   
    if (!obj.checked)
    {
    obj.checked = true;
    return;
    }
    
    var tipo = document.getElementById('feed');
    
    if (tipo.className == 'tab-link tab-link-active tab-icon')
    {
        if (obj.id == 'valorpequena'+id) 
        {   
            UncheckedControl('valormedia'+id);
            UncheckedControl('valorgrande'+id);
            
        } else if (obj.id == 'valormedia'+id) {
            UncheckedControl('valorpequena'+id);
            UncheckedControl('valorgrande'+id);
            
        } else if (obj.id == 'valorgrande'+id) {
            UncheckedControl('valorpequena'+id);
            UncheckedControl('valormedia'+id);
        }
        return;
    }
    
    tipo = document.getElementById('especiais');
};

function UncheckedControl(name){
    if (document.getElementById(name) != null)
    {
        document.getElementById(name).checked = false;
    }
};

function VerificarTamanhoSelecionado(obj, id) {
            
    expandCard("elevation" + id);
    
    var marcado = false;

    var tipo = document.getElementById('feed');
    
    if (tipo.className == 'tab-link tab-link-active tab-icon')
    {
        
        marcado = GetAnyCheck(id);
        if (marcado == false)
        {
             obj.checked = false;
        }
    }
    
    tipo = document.getElementById('especiais');
    
    if (tipo.className == 'tab-link tab-link-active')
    {
        marcado = document.getElementById('valor'+id).checked
        if (marcado == false)
        {
             obj.checked = false;
        }
    }
};

function GetAnyCheck(id){
    if (document.getElementById('valorpequena'+id) != null){
        if (document.getElementById('valorpequena'+id).checked){
            return true;
        }
    }
    
    if (document.getElementById('valormedia'+id) != null){
        if (document.getElementById('valormedia'+id).checked){
            return true;
        }
    }
    
    if (document.getElementById('valorgrande'+id) != null){
        if (document.getElementById('valorgrande'+id).checked){
            return true;
        }
    }
    
};

function Soma(id) {
                
    var elementos = null;
    var adicionar = null;
    
    var tipo = document.getElementById('feed');
    if (tipo.className == 'tab-link tab-link-active tab-icon'){
        elementos = document.getElementById('ajax_form'+id);
        adicionar = 'feedAdicionar';
    }
    
    if (elementos == null) {
        tipo = document.getElementById('especiais');
        if (tipo.className == 'tab-link tab-link-active'){
            elementos = document.getElementById('ajax_form_esfiha'+id);
            adicionar = 'especiaisAdicionar';
        }
    }
    
    var total = 0;
    
    for (var i = 0; i < elementos.length; i++){
        
        if ( elementos[i].checked) {
            total = total + getMoney(elementos[i].value);  
            
        }
    }
    
    var valor = numberToReal(total);
    
    document.getElementById(adicionar+id).innerHTML = 'Adicionar ' + valor;  
};

function getMoney( el ){  
    var money = el.replace( ',', '.' );  
    return parseFloat( money );  
} 

function numberToReal(numero) {
    var numero = numero.toFixed(2).split('.');
    numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
    return numero.join(',');
}

function getTotalCarrinho() {
    let total = Number(document.getElementById('totalCarrinho').value);
    return (total > 0);
}

function UpdateTotalPedido() {
    document.getElementById('totalPedido').innerText = document.getElementById('totalCarrinho').value;
}

function UpdateTotalCarrinho() {
    let total = Number(document.getElementById('totalCarrinho').value);
    total++;
    //document.getElementById('totalCarrinho').value = total;
    //UpdateTotalPedido();
    return true;
}

if ('serviceWorker' in navigator) {
    console.log("Will the service worker register?");
    navigator.serviceWorker.register('service-worker.js')
        .then(function (reg) {
            console.log("Yes, it did.");
        }).catch(function (err) {
            console.log("No it didn't. This happened:", err)
        });
}

var goFS = document.getElementById("goFS");
goFS.addEventListener("click", function () {
    document.body.requestFullscreen();
}, false);

var iconCarregando = $('<span class="destaque">Carregando. Por favor aguarde...</span>');
for (enviar=1; enviar<=999; enviar++){
    //alert(enviar);
    jQuery('#ajax_form'+enviar).submit(function(e) {
        e.preventDefault();
        var serializeDados = jQuery( this ).serialize();
            
        $.ajax({
            url: 'acao.php',
            dataType: 'html',
            type: 'GET',
            data: serializeDados,
            beforeSend: function() {
                $('#insere_aqui').html(iconCarregando); 
            },
            complete: function() {
                $(iconCarregando).remove(); 
            },
            success: function(data, textStatus) {
                $('#insere_aqui').html('<p>' + data + '</p>');  
            },
            error: function(xhr,er) {
                $('#mensagem_erro').html('<p class="destaque">Error ' + xhr.status + ' - ' + xhr.statusText + '<br />Tipo de erro: ' + er + '</p>')
            } 
        });     
            
    });
};

var iconCarregando1 = $('<span class="destaque">Carregando. Por favor aguarde...</span>');
for (enviar_esfiha=1; enviar_esfiha<=999; enviar_esfiha++){
    jQuery('#ajax_form_esfiha'+enviar_esfiha).submit(function(e) {
        e.preventDefault();
        var serializeDados = jQuery( this ).serialize();
            
        $.ajax({
            url: 'acao.php',
            dataType: 'html',
            type: 'GET',
            data: serializeDados,
            beforeSend: function() {
                $('#insere_aqui').html(iconCarregando1); 
            },
            complete: function() {
                $(iconCarregando1).remove(); 
            },
            success: function(data, textStatus) {
                $('#insere_aqui').html('<p>' + data + '</p>');  
            },
            error: function(xhr,er) {
                $('#mensagem_erro').html('<p class="destaque">Error ' + xhr.status + ' - ' + xhr.statusText + '<br />Tipo de erro: ' + er + '</p>')
            } 
        });     
            
    });
};

var iconCarregando2 = $('<span class="destaque">Carregando. Por favor aguarde...</span>');
var enviar_bebida = 0;
for (enviar_bebida=1; enviar_bebida<=999; enviar_bebida++){
    //alert(enviar_bebida);
    jQuery('#ajax_form_bebida'+enviar_bebida).submit(function(e) {
        e.preventDefault();
        var serializeDados = jQuery( this ).serialize();
            
        $.ajax({
            url: 'acao.php',
            dataType: 'html',
            type: 'GET',
            data: serializeDados,
            beforeSend: function() {
                $('#insere_aqui').html(iconCarregando2); 
            },
            complete: function() {
                $(iconCarregando2).remove(); 
            },
            success: function(data, textStatus) {
                $('#insere_aqui').html('<p>' + data + '</p>');  
                $('#valorsubtotal').html('<p>' + data + '</p>');
            },
            error: function(xhr,er) {
                $('#mensagem_erro').html('<p class="destaque">Error ' + xhr.status + ' - ' + xhr.statusText + '<br />Tipo de erro: ' + er + '</p>')
            } 
        });     
            
    });
};

var p = document.getElementById('valorpequena');
var m = document.getElementById('valormedia');
var g = document.getElementById('hora');

function atualizaTotal(value) {
    var totalCarrinho = document.getElementById('totalCarrinho').value;
    var total = getMoney(totalCarrinho) + getMoney(value);
    
    if (!total){
        total = 0;
    }
    document.getElementById("totalButton").innerText = 'Total: ' + numberToReal(total);
    
    var totalFinal = document.getElementById("totalFinal");
    totalFinal.innerText = 'Total: ' + numberToReal(total);
    document.getElementById("totalFinalCarrinho").value = numberToReal(total);
    
    var totalTaxa = document.getElementById("totalTaxa");
    totalTaxa.innerText = 'Taxa de Entrega: ' + numberToReal(value);
    
    document.getElementById("totalTaxaCliente").value = numberToReal(value);

    calculaTroco();
    }

function getMoney( value ){  
           var money = value.replace( ',', '.' );  
           return parseFloat( money );  
    }
    
function numberToReal(numero) {
        
        return  parseFloat(numero).toFixed(2).replace( '.', ',' );
    }

function calculaTroco() {
    
    var form = document.getElementsByClassName("list");

    var valorFormat = document.getElementById('totalFinal').innerText.replace('Total: ','').replace(',','.');

    var totalCarrinho = getMoney(valorFormat);
    var totalTroco = getMoney(form[2].elements[0].value);

    var troco= document.getElementById("totalTroco");

    if (totalTroco < totalCarrinho || isNaN(totalTroco) || isNaN(totalCarrinho)){
        troco.style.cssText = "display: none";
        return;
    }

    troco.style.cssText = "display: block";
    troco.innerText = 'Troco: ' + numberToReal(totalTroco-totalCarrinho);
}

function expandCard(id) {
    
    let obj = document.getElementById(id);
    if (obj.className.indexOf("expand") > 0){
        obj.className = "email elevation-4";
    }
    else {
        obj.className = "email elevation-4 expand"
    }
}

$(function(){
    // Executa assim que o botão de salvar for clicado
    $('#enviar').click(function(e){
        // Cancela o envio do formulário
        e.preventDefault();

        // Pega os valores dos inputs e coloca nas variáveis
        var acao = $('#acao').val();
        var categoria = $('#categoria').val();
        var img = $('#img').val();
        var cod = $('#cod').val();
        var restaurante = $('#restaurante').val();

        // Método post do Jquery
        $.get('acao.php', {
            acao:acao,
            categoria:categoria,
            img:img,
            cod:cod,
            restaurante:restaurante
        }, function(resposta){
            // Valida a resposta
            if(resposta == 1){
                // Limpa os inputs
                $('input, textarea').val('');
                alert('Mensagem enviada com sucesso.');
            }else {
                alert(resposta);
            }
        });
        
    });
});