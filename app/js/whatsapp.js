function generatePedido()
{
    GravarDadosCliente();
    
    html2canvas(document.getElementById('container')).then(function(canvas) {
        document.body.appendChild(canvas);
        
        var restaurante = document.getElementById('restaurante').value;
        var nome = document.getElementById('nome').value;        
        var telefoneCliente = document.getElementById('telefoneCliente').value;
        var rua = document.getElementById('rua').value;
        var numero = document.getElementById('numero').value;
        var cidade = document.getElementById('cidade').value;
        var select = document.getElementById('bairro');
        var bairro = select.options[select.selectedIndex].label;
        var totalTaxa = document.getElementById('totalTaxaCliente').value;
        var totalFinal = document.getElementById('totalFinalCarrinho').value;
        var totalTroco = document.getElementById('troco').value;
        if (!totalTroco){
            totalTroco = "0.00";
        }
        var cartao = document.getElementById('cartao').checked;
        if (!cartao){
            cartao = false;
        }
        var observacao = document.getElementById('observacao').value;
        if (!observacao){
            observacao = " ";
        }
        var base64URL = canvas.toDataURL('image/jpeg').replace('image/jpeg', 'image/octet-stream');
        var whatsapp = document.getElementById('telefone').value;

        // AJAX request
        $.ajax({
            url: 'ajaxfileExport.php',
            type: 'post',
            data: {
                    restaurante: restaurante,
                    nome: nome, 
                    telefoneCliente: telefoneCliente,
                    rua: rua,
                    numero: numero,
                    cidade: cidade,
                    bairro: bairro,
                    totalTaxa: totalTaxa,
                    totalFinal: totalFinal,
                    totalTroco: totalTroco,
                    cartao: cartao,
                    observacao: observacao,
                    image: base64URL, 
                    telefone: whatsapp,
                  },
            success: function(data)
            {
                console.log('Upload successfully.');
                $('#div_links').append('<a  id="btn-whats" class="link external" href="https://api.whatsapp.com/send?phone='+telefone.value+'&text=%20Esse é o meu pedido:%20https://entregas.space/app/'+data+'" ><p id="Label1">  </p>Enviar Manualmente</a>');
            },
            error: function(response){
                console.log('error: ' + JSON.stringify(response) );
            }
        });
    });
    time = 3;
    interval = setInterval(function() {
        time--;
        document.getElementById('Label1').innerHTML = "" 
        if (time == 0) {
            // stop timer
            clearInterval(interval);
            // click
            document.getElementById('btn-whats').click();            
        }
    }, 1000)
}